/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import model.Advertiser;
import model.Category;
import model.Listing;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author mousa
 */
public class CategoryParser {

    private LinkedList<Category> linkedList;
    private Map<String, Category> categories;

    JSONParser parser = new JSONParser();
    String file = "C:\\Users\\mousa\\Documents\\NetBeansProjects\\cs445\\SAD\\src\\java\\xml\\categories.json";

    public CategoryParser() throws FileNotFoundException, org.json.simple.parser.ParseException {

        categories = new HashMap<>();
        linkedList = new LinkedList<>();
        if (!file.isEmpty()) {
            parseJson();
        }
    }

    public Map<String, Category> getCategories() {
        return categories;
    }

    public LinkedList<Category> getLs() {
        return linkedList;
    }

    public void parseJson() throws FileNotFoundException, org.json.simple.parser.ParseException {
        try {
            Category tempCategory;
            Object obj = parser.parse(new FileReader(file));

            JSONArray categoryAr = (JSONArray) obj;
            JSONObject jsonObject;
            JSONObject object;
            JSONObject object1;
            JSONObject object2;
            JSONObject object3;

            Iterator<JSONObject> i = categoryAr.iterator();
            //System.out.println("i="+ i);
            while (i.hasNext()) {
                System.out.println("i=" + i);
                object = i.next();
                object3 = (JSONObject) object.get("category");
                tempCategory = new Category((String) object3.get("title"));
                jsonObject = object3;
                String title = (String) jsonObject.get("title");
                tempCategory.setTitle(title);
                long id = (Long) jsonObject.get("id");
                tempCategory.setId(id);
                JSONArray listingss = (JSONArray) jsonObject.get("listings");
                List<Listing> lists = new ArrayList<>();
                Iterator<JSONObject> iterator = listingss.iterator();
                //JSONArray catLis;
                while (iterator.hasNext()) {
                    object2 = iterator.next();
                    Listing tempListing = new Listing((String) object2.get("title"));
                    long lid = (Long) object2.get("id");
                    tempListing.setId(lid);
                    double price = (Double) object2.get("price");
                    tempListing.setPrice(price);
                    Date created = new Date((String) object2.get("created"));
                    tempListing.setStartDate(created);
                    String finished = (String) object2.get("finished");
                    tempListing.setEndDate(finished);
                    String desc = (String) object2.get("description");
                    tempListing.setDesc(desc);
                    String link = (String) object2.get("link");
                    tempListing.setLink(link);
                    String feature = (String) object2.get("feature");
                    tempListing.setFeature(feature);
                    Advertiser advertiser = new Advertiser((String) object2.get("advertiser"));
                    tempListing.setAdvertiser(advertiser);
                    tempListing.getCategories().add(tempCategory);
                    tempCategory.addListing(tempListing);
                }

                if (!this.categories.containsKey(tempCategory.getTitle()) && tempCategory.getTitle() != null) {
                    this.categories.put(tempCategory.getTitle(), tempCategory);
                    //this.categories.get(cat.getTitle()).addListing(tempListing);
                }

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void writeToJson(LinkedList<Category> ls) {
        JSONArray obj4 = new JSONArray();
        while (ls.peek() != null) {
            Category tempCategory = ls.poll();
            JSONObject obj3 = new JSONObject();
            JSONObject obj = new JSONObject();
            JSONObject obj1 = new JSONObject();
            JSONObject obj2;
            long id = tempCategory.getId();
            String title = tempCategory.getTitle();            
            List<Listing> listings = tempCategory.getListings();
            obj.put("id", id);
            obj.put("title", title);
            JSONArray list = new JSONArray();
            for (Listing lis : listings) {
                obj2 = new JSONObject();
                obj2.put("id", lis.getId());
                obj2.put("title", lis.getTitle());
                obj2.put("advertiser", lis.getAdvertiser().getProfileName());
                obj2.put("finished", lis.getEndDate());
                obj2.put("description", lis.getDesc());
                obj2.put("link", lis.getLink());
                obj2.put("price", lis.getPrice());
                obj2.put("feature", lis.getFeature());
                obj2.put("created", lis.getStartDate().toString());
                list.add(obj2);
            }
            obj.put("listings", list);
            
            obj3.put("category", obj);
            obj4.add(obj3);
        }
        try {

            FileWriter f = new FileWriter(file);
            f.write(obj4.toJSONString());
            f.flush();
            f.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}


