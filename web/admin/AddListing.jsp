<%-- 
    Document   : AddListing
    Created on : Nov 18, 2015, 9:07:21 PM
    Author     : mousa
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="../css/jumbotron-narrow.css" rel="stylesheet">
    </head>

    <body>
        <%
            
            String a ="";
            
            a = session.getAttribute("username").toString();
            
        %>
            
           
        <div class="container">
            <div class="header clearfix">
                <nav>
                    <ul class="nav nav-pills pull-right">
                        <li role="presentation" ><a href="home.php">Home</a></li>
                        <li role="presentation" class=""><a href="logout.jsp">Log out</a></li>
                    </ul>
                </nav>
            </div>

            <form action="../sad/listings" method="post">
                <div class="form-group">
                    <h1>Create New Listing</h1><br>
                    <label>Title:</label>
                    <input class="form-control" type="text" name="title" /><br>
                    <label>Price:</label>
                    <input class="form-control" type="text" name="price" /><br>
                    <label>Description:</label>
                    <textarea class="form-control" name="description" /></textarea><br>
                    <label>Link:</label>
                    <input class="form-control" type="text" name="link" /><br>
                    <label>Feature:</label>
                    <select multiple class="form-control" name="feature">
                        <option>Regular</option>
                        <option>Category-Featured</option>
                        <option>Home-Page-Featured</option>
                    </select>
                    

                    <br><br>
                    <button type="submit" class="btn btn-default">Submit</button>
                    <input type="hidden" name="submitted" />
                </div>
            </form>

        </div>
</html>
