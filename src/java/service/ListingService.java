/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import database.JsonParser;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import model.Listing;
import org.json.simple.parser.ParseException;

/**
 *
 * @author mousa
 */
public class ListingService {

    private Map<Long, Listing> listings;
    
    private LinkedList<Listing> ls;
    JsonParser parser = new JsonParser();

    public ListingService() throws FileNotFoundException, ParseException {
        listings = parser.getListings();
        ls = parser.getLs();
        
    }

    public List<Listing> getAllListings() {
        List<Listing> list = new ArrayList<Listing>(listings.values());       
        return list;
    }

    public Listing getListing(long id) {
        return listings.get(id);
    }

    public Listing addListing(Listing listing) {       
        ls.add(listing);
        parser.writeToJson(ls);
        return listing;
    }

    public void updateListing(Listing listing) {      
        int index = ls.indexOf(listing);
        ls.get(index).replace(listing);
        parser.writeToJson(ls);

    }
    

    public void removeListing(Long id) {
        ls.remove(listings.get(id));
        parser.writeToJson(ls);
    }

    public List<Listing> getAllListingsForKeyword(String keyword) {
        List<Listing> list = new ArrayList<Listing>();
        for (Listing listing : listings.values()) {
            for (String word : listing.getKeywords()) {
                if (word.equals(keyword)) {
                    list.add(listing);
                }
            }
        }
        return list;
    }


}
