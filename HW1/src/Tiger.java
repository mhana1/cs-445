
public class Tiger extends Creature{
    
    public Tiger(String name){
        super(name);
    }
    
    public void move(){
        System.out.println(super.getName() + " " + super.getClass().getSimpleName() + " has just pounced");
    }
}
