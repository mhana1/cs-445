/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package service;

import database.JsonParser;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import model.Advertiser;
import model.Category;
import model.Listing;
import org.json.simple.parser.ParseException;

/**
 *
 * @author mousa
 */
public class AdvertiserService {
    
    JsonParser parser =new JsonParser();
    private Map<Long, Advertiser> advertisers ;
    
    public AdvertiserService()throws FileNotFoundException, ParseException{
        advertisers = parser.getAdvertisers();
    }
    
    public List<Advertiser> getAllAdvertisers(){        
        return new ArrayList<Advertiser>(advertisers.values());
    }
    
    public Advertiser getAdvertiser(long id){
        return advertisers.get(id);
    }
    
   /* public Advertiser addAdvertiser(Advertiser advertiser){
        advertiser.setId(advertisers.size() + 1);
        advertisers.put(advertiser.getProfileName(), advertiser);
        return advertiser;
    }
    
    public Advertiser updateAdvertiser(Advertiser advertiser){
        if (advertiser.getProfileName().isEmpty()){
            return null;
        }
        advertisers.put(advertiser.getProfileName(), advertiser);
        return advertiser;
    }*/
    
    
    
    public List<Advertiser> searchAdvertiser(String keyword){
        //long phone = Long.valueOf(keyword).longValue();
        List<Advertiser> adList = new ArrayList<>();
        Advertiser advertiser;
        for(Advertiser adv: advertisers.values()){
            for(Listing lis : adv.getListings()){
                if(lis.getTitle().equalsIgnoreCase(keyword)){
                    adList.add(adv);
                }
            }
            if(adv.getEmail().equalsIgnoreCase(keyword) || adv.getLastName().equalsIgnoreCase(keyword) || String.valueOf(adv.getPhone()).equals(keyword)){
                adList.add(adv);
            }
            
            if(!adv.getFacebook().equals("")){
            if (adv.getFacebook().equalsIgnoreCase(keyword)){
                adList.add(adv);
            }
            }
            
            if(!adv.getTwitter().equals("")){
            if (adv.getTwitter().equalsIgnoreCase(keyword)){
                adList.add(adv);
            }
            }
        }
        return adList;
    }
    
    
}
