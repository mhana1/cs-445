

/**
 *
 * @author mousa
 */

public class TestCreature {
    
    public static final int CREATURE_COUNT = 6;
    public static final int THING_COUNT = 10;
    
    public TestCreature(){
        
    }          
    
 public static void main(String[] args){
     int iterator = 0;       
     Thing[] things = new Thing[THING_COUNT];   
     things[iterator++] = new Thing("Banana");
     things[iterator++] = new Thing("Apple Juice");
     things[iterator++] = new Tiger("Tigora");
     things[iterator++] = new Tiger("Togo");
     things[iterator++] = new Thing("");
     
     Creature[] creatures = new Creature[CREATURE_COUNT];
     creatures[0] = new Bat("Foxie");
     creatures[1] = new Ant("Patch");
     creatures[2] = new Fly("wzz");
     creatures[3] = new Tiger("trump");   
     
     
     for (int i=0; i<=(THING_COUNT-(iterator-1)); i++){
         things[iterator++] = creatures[i];
     }
          
    System.out.println("Things:\n");    
    for (int i=0; i<things.length; i++){
         System.out.println(things[i]);
     }
        
    System.out.println("\nCreatures:\n");
    for (int i=0; i<creatures.length; i++){
         System.out.println(creatures[i]);
     }      
     
    for (int i=0; i<creatures.length; i++){
        if (creatures[i] != null)
        creatures[i].move();
        
    }
    Tiger tigo = new Tiger("tsad");
    tigo.eat(things[1]);
    
    
 }  
 
}
