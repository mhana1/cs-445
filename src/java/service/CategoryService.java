/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package service;

import database.JsonParser;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import model.Category;
import model.Listing;
import org.json.simple.parser.ParseException;

/**
 *
 * @author mousa
 */
public class CategoryService {
    JsonParser parser =new JsonParser();
    private Map<String,Category> categories ;
    
    public CategoryService() throws FileNotFoundException, ParseException{       
        categories = parser.getCategories();
    }
    
    public List<Category> getAllCategories(){        
        return new ArrayList<Category>(categories.values());
    }
    
    public Category getCategory(String title){
        return categories.get(title);
    }
    
    public Category addCategory(Category category){
        //category.setId(categories.size() + 1);
        categories.put(category.getTitle(), category);
        return category;
    }
    
    public Category updateCategory(Category category){
        if (category.getId() <= 0){
            return null;
        }
        categories.put(category.getTitle(), category);
        return category;
    }
    
    public Category removeCategory(Long id){
        return categories.remove(id);
    }
    
}
