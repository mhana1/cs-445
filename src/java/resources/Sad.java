/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resources;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import model.Category;
import model.Listing;
import org.json.simple.parser.ParseException;
import service.CategoryService;
import service.ListingService;

/**
 *
 * @author mousa
 */
@Path("/directory")
public class Sad {

    CategoryService categoryService;
    ListingService listingService;

    String html = "<!DOCTYPE html>\n"
            + "<html>\n"
            + "    <head>\n"
            + "        <meta charset=\"utf-8\">\n"
            + "        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n"
            + "        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n"
            + "        <!-- Bootstrap core CSS -->\n"
            + "        <link href=\"http://localhost:8080/css/bootstrap.min.css\" rel=\"stylesheet\">\n"
            + "\n"
            + "        <!-- Custom styles for this template -->\n"
            + "        <link href=\"http://localhost:8080/css/jumbotron-narrow.css\" rel=\"stylesheet\">\n"
            + "        <title>Special Advertising Directory</title>\n"
            + "<style> .lefto{float:left}</style>"
            + "    </head>\n"
            + "\n"
            + "    <body>\n"
            + "        <div class=\"container\">\n"
            + "            <div class=\"header clearfix\">\n"
            + "                <nav>\n"
            + "                    <ul class=\"nav nav-pills pull-right\">\n"
            + "                        <li role=\"presentation\" class=\"\"><a href=\"http://localhost:8080/sad/directory\">Home</a></li>\n"
            + "                    </ul>\n"
            + "                </nav>\n"
            + "            </div>\n";

    String search = "            <a href=\"\"><h1 class=\"left\">sad</h1></a>\n"
            + "            <h2 class=\"right\">Special Advertising Directory</h2>\n"
            + "\n"
            + "            <div class=\"jumbotron\">    <div class=\"col-left\">\n"
            + "                    <h3 class=\"sprite search-head\"><b>Search the Directory</h3>\n"
            + "                    <form id=\"search-form-tray\" name=\"search-form-tray\" action=\"http://localhost:8080/sad/directory/\" method=\"post\" accept-charset=\"utf-8\">\n"
            + "                        <label>By Keywords</label><br>\n"
            + "                        <input class=\"form-control\" type=\"text\" name=\"keyword\" value=\"\" id=\"searchText\" /><br><br>\n"
            + "                        <label>By Category</label><br>\n"
            + "                        <select class=\"form-control\" name=\"category\">"
            + "<option> </option>"
            + "                            \n";

    String end = "               </div>\n"
            + "            <button type=\"submit\" class=\"btn btn-default\">Search</button>\n"
            + "    </div></body></html>\n";

    @GET
    @Produces(MediaType.TEXT_HTML)
    public String getDirectory() throws FileNotFoundException, ParseException {
        categoryService = new CategoryService();
        html += search;
        for (Category cat : categoryService.getAllCategories()) {
            html += "<option>" + cat.getTitle() + "</option>";
        }
        return html + "</select></div>" + end;
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_HTML)
    public String filter(@FormParam("keyword") String keyword,
            @FormParam("category") String category) throws UnsupportedEncodingException {

        if (keyword.equals("") && !category.equals("")) {
            return "<!DOCTYPE html>\n"
                    + "<html>\n"
                    + "<head>\n"
                    + "   <!-- HTML meta refresh URL redirection -->\n"
                    + "   <meta http-equiv=\"refresh\"\n"
                    + "   content=\"0; url=http://localhost:8080/sad/directory/search?category=" + URLEncoder.encode(category, "UTF-8") + "&&keyword=\">\n"
                    + "</head>\n"
                    + "</html>";
        } else if (!keyword.equals("") && category.equals("")) {
            return "<!DOCTYPE html>\n"
                    + "<html>\n"
                    + "<head>\n"
                    + "   <!-- HTML meta refresh URL redirection -->\n"
                    + "   <meta http-equiv=\"refresh\"\n"
                    + "   content=\"0; url=http://localhost:8080/sad/directory/search?keyword=" + keyword + "&&category=\">\n"
                    + "</head>\n"
                    + "</html>";
        }

        return error();
    }

    @GET
    @Path("/search")
    @Produces(MediaType.TEXT_HTML)
    public String searchBy(@QueryParam("keyword") String keyword,
            @QueryParam("category") String category) throws FileNotFoundException, ParseException, UnsupportedEncodingException {
        listingService = new ListingService();
        categoryService = new CategoryService();
       // html += "<div class=\"jumbotron\">";
        System.out.println(category);
        category = URLDecoder.decode(category, "UTF-8");
        System.out.println(category);
        if (!keyword.equals("") && category.equals("")) {
            List<Listing> lis = listingService.getAllListingsForKeyword(keyword);
            if (lis.isEmpty()) {
                return error();
            } else {
                html += "<h2 class='lefto'><b>Results for: '" + keyword + "'</h2><br>" + "<table class='table'><thead>"
                        + "        <tr>\n"
                        + "            <th>Title</th>\n"
                        + "            <th>Advertiser</th>\n"
                        + "            <th>Price</th>\n"
                        + "        </tr>\n"
                        + "        </thead>\n"
                        + "        <tbody>";
                for (Listing listing : lis) {
                    html += "<tr><th><a href='http://localhost:8080/sad/directory/listings/" + listing.getId() + "'>" + listing.getTitle() + "</th><th>" + listing.getAdvertiser().getFirstName() + " " + listing.getAdvertiser().getLastName() + "</th><th>" + listing.getPrice() + "</th></tr>";
                }
                html += "</table>";
                return html;
            }

        } else if (keyword.equals("") && !category.equals("")) {
            List<Listing> list = categoryService.getCategory(category).getListings();
            if (list.isEmpty()) {
                return error();
            } else {
                html += "<h2 class='lefto'><b>Category: "+category+"</h2><br><br><table class='table'><thead>"
                        + "        <tr>\n"
                        + "            <th>Title</th>\n"
                        + "            <th>Advertiser</th>\n"
                        + "            <th>Price</th>\n"
                        + "        </tr>\n"
                        + "        </thead>\n"
                        + "        <tbody>";
                for (Listing listing : list) {
                    if(listing.getFeature().equalsIgnoreCase("category-featured")){
                        html += " <tr class=\"success\"><th><a href='http://localhost:8080/sad/directory/listings/" + listing.getId() + "'>" + listing.getTitle() + "</th><th>" + listing.getAdvertiser().getFirstName() + " " + listing.getAdvertiser().getLastName() + "</th><th>" + listing.getPrice() + "</th></tr>";
                    }
                    else{
                    html += "<tr><th><a href='http://localhost:8080/sad/directory/listings/" + listing.getId() + "'>" + listing.getTitle() + "</th><th>" + listing.getAdvertiser().getFirstName() + " " + listing.getAdvertiser().getLastName() + "</th><th>" + listing.getPrice() + "</th></tr>";
                    
                    }
                }
                html += "</table>";
                return html;
            }

        }

        return error();
    }

    @GET
    @Path("/search/error")
    @Produces(MediaType.TEXT_HTML)
    public String error() {
        return "<!DOCTYPE html>\n"
                + "<html> \n"
                + "    <head>\n"
                + "        <meta charset=\"utf-8\">\n"
                + "        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n"
                + "        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n"
                + "        <!-- Bootstrap core CSS -->\n"
                + "        <link href=\"http://localhost:8080/css/bootstrap.min.css\" rel=\"stylesheet\">\n"
                + "\n"
                + "        <!-- Custom styles for this template -->\n"
                + "        <link href=\"http://localhost:8080/css/jumbotron-narrow.css\" rel=\"stylesheet\">\n"
                + "        <title>Login Page</title>\n"
                + "    </head> \n"
                + "    <body>\n"
                + "        <div class=\"container\">\n"
                + "            <div class=\"header clearfix\">\n"
                + "                <nav>\n"
                + "                    <ul class=\"nav nav-pills pull-right\">\n"
                + "                        <li role=\"presentation\" class=\"\"><a href=\"http://localhost:8080/sad/directory\">Home</a></li>\n"
                + "                    </ul>\n"
                + "                </nav>\n"
                + "            </div>\n"
                + "        <h1>Sorry, No results found! </h1><p>Try to search by ethier keyword or category<br><br><a href=\"http://localhost:8080/sad/directory\"><button type=\"submit\" class=\"btn btn-default\">Go Back</button></html>";
    }

}
