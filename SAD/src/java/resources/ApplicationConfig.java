/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package resources;

import resources.*;
import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author mousa
 */
@javax.ws.rs.ApplicationPath("sad")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        resources.add(resources.AdminListingResource.class);
        resources.add(resources.AdvertiserResource.class);
        resources.add(resources.CategoryResource.class);
        resources.add(resources.ReportResource.class);
        resources.add(resources.Sad.class);
        resources.add(resources.CSRResource.class);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(resources.AdminListingResource.class);
        resources.add(resources.AdvertiserResource.class);
        resources.add(resources.CSRResource.class);
        resources.add(resources.CategoryResource.class);
        resources.add(resources.ReportResource.class);
        resources.add(resources.Sad.class);
    }
    
}
