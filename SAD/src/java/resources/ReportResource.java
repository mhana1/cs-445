/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resources;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import model.Category;
import model.Listing;
import org.json.simple.parser.ParseException;
import service.CategoryService;
import service.ListingService;

/**
 *
 * @author mousa
 */
@Path("/admin/report")
public class ReportResource {

    String html = "<!DOCTYPE html>\n"
            + "<html>\n"
            + "    <head>\n"
            + "        <meta charset=\"utf-8\">\n"
            + "        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n"
            + "        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n"
            + "        <!-- Bootstrap core CSS -->\n"
            + "        <link href=\"http://localhost:8080/css/bootstrap.min.css\" rel=\"stylesheet\">\n"
            + "\n"
            + "        <!-- Custom styles for this template -->\n"
            + "        <link href=\"http://localhost:8080/css/jumbotron-narrow.css\" rel=\"stylesheet\">\n"
            + "        <title>Special Advertising Directory</title>\n"
            + "    </head>\n"
            + "\n"
            + "    <body>\n"
            + "        <div class=\"container\">\n"
            + "            <div class=\"header clearfix\">\n"
            + "                <nav>\n"
            + "                    <ul class=\"nav nav-pills pull-right\">\n"
            + "                        <li role=\"presentation\" class=\"\"><a href=\"http://localhost:8080/sad/admin/report\">Home</a></li>\n"
            + "                    </ul>\n"
            + "                </nav>\n"
            + "            </div>"
            + " <div class=\"jumbotron\"><br><h1>Reports</h1><br><br>";

    ListingService listingService;
    CategoryService categoryService;

    @GET
    @Produces(MediaType.TEXT_HTML)
    public String getListings() throws FileNotFoundException, ParseException {
        
            html += "<h3><a href=\"http://localhost:8080/sad/admin/report/1\">Active Listings report</a><br>";
            html += "<h3><a href=\"http://localhost:8080/sad/admin/report/2\">Customers report</a><br><br>";
            
            return html;
    }

    
    @GET
    @Path("/{reportId}")
    @Produces(MediaType.TEXT_HTML)
    public String getListing(@PathParam("reportId") int id) throws FileNotFoundException, ParseException {
        listingService = new ListingService();
        categoryService = new CategoryService();
       
        if(id ==1 ){
             html += "<table class=\"table\"><thead>\n"
                    + "        <tr>\n"
                    + "            <th>Category</th>\n"
                    + "            <th># of active listings</th>\n"
                    + "        </tr>\n"
                    + "        </thead>\n"
                    + "        <tbody>";
             int num=0;
             for (Category category : categoryService.getAllCategories()) {
                for(Listing lis: category.getListings()){
                 if(lis.getEndDate().equalsIgnoreCase("still available")){
                     num++;
                 }
                }             
                html += "<tr><th>" + category.getTitle() + "</th><th>"+ num+"</th><th></th></tr><thead>";
                
            }
             
             return html;
             
        }
        
        else if(id == 2){
            
        }
        
        
        return error();
    }
    
    
     @GET
    @Path("/error")
    @Produces(MediaType.TEXT_HTML)
    public String error() {
        return "<!DOCTYPE html>\n"
                + "<html> \n"
                + "    <head>\n"
                + "        <meta charset=\"utf-8\">\n"
                + "        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n"
                + "        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n"
                + "        <!-- Bootstrap core CSS -->\n"
                + "        <link href=\"http://localhost:8080/css/bootstrap.min.css\" rel=\"stylesheet\">\n"
                + "\n"
                + "        <!-- Custom styles for this template -->\n"
                + "        <link href=\"http://localhost:8080/css/jumbotron-narrow.css\" rel=\"stylesheet\">\n"
                + "        <title>Login Page</title>\n"
                + "    </head> \n"
                + "    <body>\n"
                + "        <div class=\"container\">\n"
                + "            <div class=\"header clearfix\">\n"
                + "                <nav>\n"
                + "                    <ul class=\"nav nav-pills pull-right\">\n"
                + "                        <li role=\"presentation\" class=\"\"><a href=\"http://localhost:8080/sad/directory\">Home</a></li>\n"
                + "                    </ul>\n"
                + "                </nav>\n"
                + "            </div>\n"
                + "        <h1>Sorry, No reports found! </h1><p>Try to search by ethier keyword or category<br><br><a href=\"http://localhost:8080/sad/admin/report\"><button type=\"submit\" class=\"btn btn-default\">Go Back</button></html>";
    }

}
