<%-- 
    Document   : admin
    Created on : Nov 18, 2015, 6:36:13 PM
    Author     : mousa
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="../css/jumbotron-narrow.css" rel="stylesheet">
        <title>Login Page</title>
    </head>
    <body>
        <div class="container">
            <div class="header clearfix">
                <nav>
                    <ul class="nav nav-pills pull-right">
                        <li role="presentation" class=""><a href="index.jsp">Home</a></li>
                    </ul>
                </nav>
            </div>
            <h1>Please Login</h1><br>
        <form action="LoginCheck.jsp" method="post">
            <div class="form-group">
                <label for="exampleInputEmail1">Username</label>
                <input type="name" name="username" class="form-control" id="exampleInputEmail1" placeholder="Username">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
            </div>
            <br>
            <button type="submit" class="btn btn-default">Submit</button>
           
        </form>
        </div>
    </body>
</html>

