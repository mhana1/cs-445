<%-- 
    Document   : controller
    Created on : Nov 18, 2015, 7:56:45 PM
    Author     : mousa
--%>

<%@page import="java.util.List"%>
<%@page import="model.Listing"%>
<%@page import="service.ListingService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/jumbotron-narrow.css" rel="stylesheet">
    <style>

    </style>
</head>

<body>
<div class="container">
    <div class="header clearfix">
        <nav>
            <ul class="nav nav-pills pull-right">
                <li role="presentation" class=""><a href="home.jsp">Home</a></li>
                <li role="presentation" class=""><a href="logout.jsp">Log out</a></li>
            </ul>
        </nav>
    </div>

    <a id ="add" type="button" class="btn btn-danger" href="AddCategory.jsp">+Add Category</a>
    <a id ="add" type="button" class="btn btn-danger" href="AddListing.jsp">+Add Listing</a>
    <div class="jumbotron">
        <%
            
            String a ="";
            
            a = session.getAttribute("username").toString();
            ListingService listingService = new ListingService();            
            List<Listing> list = listingService.getAllListings();
        %>
        <br><h1>Listings</h1><br><br>

    <div class="container">
    <table class="table">
        <thead>
            <%
            for(Listing listing: list){%>
                <tr>
                    <th><% listing.getTitle(); %></th>
                </tr>
                <%
            }
            %>
        </thead>
        <tbody>
            
        </tbody>
    </table>
    </div>
        
