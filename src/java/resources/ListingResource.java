/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resources;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import model.Listing;
import org.json.simple.parser.ParseException;
import service.ListingService;

/**
 *
 * @author mousa
 */
@Path("/directory/listings")
public class ListingResource {

    String html = "<!DOCTYPE html>\n"
            + "<html>\n"
            + "    <head>\n"
            + "        <meta charset=\"utf-8\">\n"
            + "        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n"
            + "        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n"
            + "        <!-- Bootstrap core CSS -->\n"
            + "        <link href=\"http://localhost:8080/css/bootstrap.min.css\" rel=\"stylesheet\">\n"
            + "\n"
            + "        <!-- Custom styles for this template -->\n"
            + "        <link href=\"http://localhost:8080/css/jumbotron-narrow.css\" rel=\"stylesheet\">\n"
            + "        <title>Special Advertising Directory</title>\n"
            + "    </head>\n"
            + "\n"
            + "    <body>\n"
            + "        <div class=\"container\">\n"
            + "            <div class=\"header clearfix\">\n"
            + "                <nav>\n"
            + "                    <ul class=\"nav nav-pills pull-right\">\n"
            + "                        <li role=\"presentation\" class=\"\"><a href=\"http://localhost:8080/sad/directory\">Home</a></li>\n"
            + "                    </ul>\n"
            + "                </nav>\n"
            + "            </div>"
            + " <div class=\"jumbotron\"><br><h1>Listings</h1><br><br>";

    ListingService listingService;

    @GET
    @Produces(MediaType.TEXT_HTML)
    public String getListings() throws FileNotFoundException, ParseException {
        listingService = new ListingService();
        List<Listing> list = listingService.getAllListings();
        if (!list.isEmpty()) {
            html += "<table class=\"table\"><thead>\n"
                    + "        <tr>\n"
                    + "            <th>Title</th>\n"
                    + "            <th>Advertiser</th>\n"
                    + "            <th>Price</th>\n"
                    + "        </tr>\n"
                    + "        </thead>\n"
                    + "        <tbody>";
            for (Listing listing : list) {
                html += "<tr><th><a href='http://localhost:8080/sad/directory/listings/" + listing.getId() + "'>" + listing.getTitle() + "</th><th>" + listing.getAdvertiser().getFirstName() + " " + listing.getAdvertiser().getLastName() + "</th><th>" + listing.getPrice() + "</th></tr>";
            }
        } else {
            html += "<p>No Listings Found!</p>";
        }
        return html += "</tbody></table>";
    }

    
    @GET
    @Path("/{listingId}")
    @Produces(MediaType.TEXT_HTML)
    public String getListing(@PathParam("listingId") long id) throws FileNotFoundException, ParseException {
        listingService = new ListingService();
        Listing ls = listingService.getListing(id);
        return "<!DOCTYPE html>\n"
                + "<html lang=\"en\">\n"
                + "  <head>\n"
                + "    <meta charset=\"utf-8\">\n"
                + "    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n"
                + "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n"
                + "    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->\n"
                + "    <meta name=\"description\" content=\"\">\n"
                + "    <meta name=\"author\" content=\"\">\n"
                + "    <link rel=\"icon\" href=\"../../favicon.ico\">\n"
                + "\n"
                + "    <title>Blog Template for Bootstrap</title>\n"
                + "\n"
                + "    <!-- Bootstrap core CSS -->\n"
                + "    <link href=\"http://localhost:8080/css/bootstrap.min.css\" rel=\"stylesheet\">\n"
                + "\n<link href=\"http://localhost:8080/css/jumbotron-narrow.css\" rel=\"stylesheet\">\n"
                + "    <!-- Custom styles for this template -->\n"
                + "    <link href=\"http://localhost:8080/css/blog.css\" rel=\"stylesheet\">\n"
                + "  </head>\n"
                + "\n"
                + "  <body>\n"
                + "\n"
                + "\n"
                + "    <div class=\"container\">\n"
                + "\n<div class=\"header clearfix\">\n"
                + "                <nav>\n"
                + "                    <ul class=\"nav nav-pills pull-right\">\n"
                + "                        <li role=\"presentation\" class=\"\"><a href=\"http://localhost:8080/sad/directory\">Home</a></li>\n"
                + "                    </ul>\n"
                + "                </nav>\n"
                + "            </div>"
                + "      <div class=\"blog-header\">\n"
                + "        <h1 class=\"blog-title\">" + ls.getTitle() + "</h1>\n"
                + "        <p class=\"lead blog-description\">"+ ls.getStartDate().toString() + " by " + ls.getAdvertiser().getFirstName() + " " + ls.getAdvertiser().getLastName() + "</a></p>\n"
                + "      </div>\n"
                + "\n"
                + "      <div class=\"row\">\n"
                + "\n"
                + "        <div class=\"col-sm-8 blog-main\">\n"
                + "\n"
                + "          <div class=\"blog-post\">\n"
                  + "<p>" + ls.getDesc() + "</p>"
                + " <p>Website: " + "<a href='" + ls.getLink() + "'>" + ls.getLink()
                + "      </div><!-- /.row -->\n"
                + "    </div><!-- /.container -->\n"
                + "  </body>\n"
                + "</html>";
    }

}
