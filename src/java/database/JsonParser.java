/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import model.Advertiser;
import model.Category;
import model.Listing;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import resources.Sad;

/**
 *
 * @author mousa
 */
public class JsonParser {

    private Map<Long, Listing> listings;
    private LinkedList<Listing> listingList;
    private Map<String, Category> categories;
    private Map<Long, Advertiser> advertisers;
    JSONParser parser = new JSONParser();
    String file = "C:\\Users\\mousa\\Documents\\NetBeansProjects\\cs445\\src\\java\\xml\\listings.json";

    public JsonParser() throws FileNotFoundException, org.json.simple.parser.ParseException {
        listings = new HashMap<>();
        categories = new HashMap<>();
        advertisers = new HashMap<>();
        listingList = new LinkedList<>();
        if (!file.isEmpty()) {
            parseJson();
        }
    }

    public Map<Long, Listing> getListings() {
        return listings;
    }

    public Map<String, Category> getCategories() {
        return categories;
    }
    
    public Map<Long, Advertiser> getAdvertisers() {
        return advertisers;
    }

    public LinkedList<Listing> getLs() {
        return listingList;
    }

    public void parseJson() throws FileNotFoundException, org.json.simple.parser.ParseException {
        try {
            Listing tempListing;
            Object obj = parser.parse(new FileReader(file));

            JSONArray listingAr = (JSONArray) obj;
            JSONObject jsonObject;
            JSONObject object;
            JSONObject object1;
            JSONObject object2;
            JSONObject object3;

            Iterator<JSONObject> i = listingAr.iterator();
            //System.out.println("i="+ i);
            while (i.hasNext()) {
                System.out.println("i=" + i);
                object = i.next();
                object3 = (JSONObject) object.get("listing");
                tempListing = new Listing((String) object3.get("title"));
                jsonObject = object3;
                String title = (String) jsonObject.get("title");
                tempListing.setTitle(title);
                long id = (Long) jsonObject.get("id");
                tempListing.setId(id);
                double price = (Double) jsonObject.get("price");
                tempListing.setPrice(price);
                Date created = new Date((String) jsonObject.get("created"));
                tempListing.setStartDate(created);
                String finished = (String) jsonObject.get("finished");
                tempListing.setEndDate(finished);
                String desc = (String) jsonObject.get("description");
                tempListing.setDesc(desc);
                String link = (String) jsonObject.get("link");
                tempListing.setLink(link);
                String feature = (String) jsonObject.get("feature");
                tempListing.setFeature(feature);
                object1 = (JSONObject) jsonObject.get("advertiser");
                Advertiser advertiser = new Advertiser((String) object1.get("profileName"));
                advertiser.setId((long) object1.get("id"));
                advertiser.setEmail((String) object1.get("email"));
                advertiser.setFirstName((String) object1.get("firstName"));
                advertiser.setLastName((String) object1.get("lastName"));
                advertiser.setFacebook((String) object1.get("facebook"));
                advertiser.setTwitter((String) object1.get("twitter"));
                advertiser.setPhone((long) object1.get("phone"));
                advertiser.addListing(tempListing);
                advertisers.put(advertiser.getId(), advertiser);
                System.out.println(advertiser.getLastName());
                tempListing.setAdvertiser(advertiser);
                JSONArray keywords = (JSONArray) jsonObject.get("keywords");
                List<String> words = new ArrayList<>();
                if (!keywords.isEmpty()) {
                    Iterator<String> iter = keywords.iterator();
                    while (iter.hasNext()) {
                        words.add(iter.next());
                    }
                    tempListing.setKeywords(words);
                }
                JSONArray categories = (JSONArray) jsonObject.get("categories");
                List<Category> cats = new ArrayList<>();
                Iterator<JSONObject> iterator = categories.iterator();
                //JSONArray catLis;
                while (iterator.hasNext()) {
                    object2 = iterator.next();
                    Category cat = new Category((String) object2.get("title"), tempListing);
                    /*catLis = (JSONArray) object2.get("listings");
                     Iterator<String> j = catLis.iterator();
                     while(j.hasNext()){
                     cat.addListing(getListings().get(j.next()));
                     }*/
                    
                    cats.add(cat);
                    if (!this.categories.containsKey(cat.getTitle()) && cat.getTitle() != null) {
                        this.categories.put(cat.getTitle(), cat);
                    } 
                    else{ this.categories.get(cat.getTitle()).addListing(tempListing);
                    }
                    
                }
                tempListing.setCategories(cats);

                this.listings.put(tempListing.getId(), tempListing);
                this.listingList.add(tempListing);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void writeToJson(LinkedList<Listing> ls) {
        JSONArray obj4 = new JSONArray();
        while (ls.peek() != null) {
            Listing listing = ls.poll();
            JSONObject obj3 = new JSONObject();
            JSONObject obj = new JSONObject();
            JSONObject obj1 = new JSONObject();
            JSONObject obj2;
            long id = listing.getId();
            String title = listing.getTitle();
            double price = listing.getPrice();
            Date created = listing.getStartDate();
            String finished = listing.getEndDate();
            String desc = listing.getDesc();
            String link = listing.getLink();
            List<Category> categories = listing.getCategories();
            String feature = listing.getFeature();
            Advertiser advertiser = listing.getAdvertiser();
            List<String> keywords = listing.getKeywords();

            obj.put("id", id);
            obj.put("title", title);
            obj.put("price", price);
            obj.put("created", created.toString());
            obj.put("feature", feature);
            obj1.put("id", advertiser.getId());
            obj1.put("profileName", advertiser.getProfileName());
            obj1.put("firstName", advertiser.getFirstName());
            obj1.put("lastName", advertiser.getLastName());
            obj1.put("email", advertiser.getEmail());
            obj1.put("phone", advertiser.getPhone());
            obj1.put("facebook", advertiser.getFacebook());
            obj1.put("twitter", advertiser.getTwitter());
            obj.put("advertiser", obj1);
            obj.put("finished", finished.toString());
            obj.put("description", desc);
            obj.put("link", link);
            JSONArray list1 = new JSONArray();
            for (String word : keywords) {
                list1.add(word);
            }
            obj.put("keywords", list1);

            JSONArray list = new JSONArray();
            JSONArray catList = new JSONArray();
            catList.add(obj.get("title"));
            for (Category cat : categories) {
                obj2 = new JSONObject();
                obj2.put("id", cat.getId());
                obj2.put("title", cat.getTitle());
                //obj2.put("listings", catList);
                list.add(obj2);
            }
            obj.put("categories", list);
            obj3.put("listing", obj);
            obj4.add(obj3);
        }
        try {

            FileWriter f = new FileWriter(file);
            f.write(obj4.toJSONString());
            f.flush();
            f.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
