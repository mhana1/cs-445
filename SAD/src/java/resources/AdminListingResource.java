/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resources;


import java.io.FileNotFoundException;
import java.util.Date;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import model.Advertiser;
import model.Category;
import model.Listing;
import org.json.simple.parser.ParseException;
import service.CategoryService;
import service.ListingService;

/**
 *
 * @author mousa
 */
@Path("/admin/listing")
public class AdminListingResource {

    static boolean logged = false;

    String html = "<!DOCTYPE html>\n"
            + "<html>\n"
            + "    <head>\n"
            + "        <meta charset=\"utf-8\">\n"
            + "        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n"
            + "        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n"
            + "        <!-- Bootstrap core CSS -->\n"
            + "        <link href=\"http://localhost:8080/css/bootstrap.min.css\" rel=\"stylesheet\">\n"
            + "\n"
            + "        <!-- Custom styles for this template -->\n"
            + "        <link href=\"http://localhost:8080/css/jumbotron-narrow.css\" rel=\"stylesheet\">\n"
            + "        <title>Special Advertising Directory</title>\n"
            + "    </head>\n"
            + "\n"
            + "    <body>\n"
            + "        <div class=\"container\">\n"
            + "            <div class=\"header clearfix\">\n"
            + "                <nav>\n"
            + "                    <ul class=\"nav nav-pills pull-right\">\n"
            + "                        <li role=\"presentation\" class=\"\"><a href=\"http://localhost:8080/sad/admin/reports\">Report</a></li>\n"
            + "                        <li role=\"presentation\" class=\"\"><a href=\"http://localhost:8080/sad/admin/listing\">Home</a></li></ul></nav></div>";

    String form = "                </ul></nav></div>\n<h1>Please Login</h1><br>\n"
            + "        <form action=\"http://localhost:8080/sad/admin\" method=\"post\">\n"
            + "            <div class=\"form-group\">\n"
            + "                <label for=\"exampleInputEmail1\">Username</label>\n"
            + "                <input type=\"name\" name=\"username\" class=\"form-control\" id=\"exampleInputEmail1\" placeholder=\"Username\">\n"
            + "            </div>\n"
            + "            <div class=\"form-group\">\n"
            + "                <label for=\"exampleInputPassword1\">Password</label>\n"
            + "                <input type=\"password\" name=\"password\" class=\"form-control\" id=\"exampleInputPassword1\" placeholder=\"Password\">\n"
            + "            </div>\n"
            + "            <br>\n"
            + "            <button type=\"submit\" class=\"btn btn-default\">Submit</button>\n"
            + "           \n"
            + "        </form>\n"
            + "        </div>\n"
            + "    </body>\n"
            + "</html>";

    ListingService listingService;
    CategoryService categoryService;

   

    @GET
    @Produces(MediaType.TEXT_HTML)
    public String getHome() throws FileNotFoundException, ParseException {
        listingService = new ListingService();
        categoryService = new CategoryService();
       
            html += "<div> <a id =\"add\" type=\"button\" class=\"btn btn-danger\" href=\"http://localhost:8080/sad/admin/listing/advertiserInfo\">+Add Listing</a></div><br>";
            List<Listing> lists = listingService.getAllListings();
            List<Category> cats = categoryService.getAllCategories();
            html += "<table class=\"table\"><thead>\n"
                    + "        <tr>\n"
                    + "            <th>Category</th>\n"
                    + "            <th>Listing Title</th>\n"
                    + "            <th>Advertiser</th>\n"
                    + "        </tr>\n"
                    + "        </thead>\n"
                    + "        <tbody>";
            for (Category category : cats) {
                html += "<thead><tr><th>" + category.getTitle() + "</th><th></th><th></th></tr><thead>";
                for (Listing listing : category.getListings()) {
                    html += "<tr><td></td><td><a href='http://localhost:8080/sad/admin/listing/" + listing.getId() + "'>" + listing.getTitle() + "</td><td>" + listing.getAdvertiser().getFirstName() + " " + listing.getAdvertiser().getLastName() + "</tr>";
                }
            }
            return html + "</tbody></table>";
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_HTML)
    public String addListing(@FormParam("title") String title,
            @FormParam("price") double price,
            @FormParam("description") String description,
            @FormParam("link") String link,
            @FormParam("category") String categ,
            @FormParam("feature") String feature,
            @FormParam("profileName") String profile,
            @FormParam("firstName") String first,
            @FormParam("lastName") String last,
            @FormParam("email") String email,
            @FormParam("facebook") String facebook,
            @FormParam("twitter") String twitter,
            @FormParam("phone") long phone) throws FileNotFoundException, ParseException, NullPointerException {
        
            categoryService = new CategoryService();
            listingService = new ListingService();
            Advertiser adver = new Advertiser(profile);
            adver.setEmail(email);
            adver.setFirstName(first);
            adver.setLastName(last);
            adver.setPhone(phone);
            adver.setFacebook(facebook);
            adver.setTwitter(twitter);
            Listing listing = new Listing(title, price, description, link, adver);
            listing.setFeature(feature);
            Category tempCategory = categoryService.getCategory(categ);

            if (tempCategory != null) {
                listing.getCategories().add(tempCategory);
                tempCategory.addListing(listing);
            } else {
                tempCategory = new Category(categ, listing);
                listing.getCategories().add(tempCategory);
            }
            listingService.addListing(listing);
            return getHome();
        
    }
    
    @GET
    @Path("/{listingId}")
    @Produces(MediaType.TEXT_HTML)
    public String getListing(@PathParam("listingId") long id) throws FileNotFoundException, ParseException {
        listingService = new ListingService();
        Listing ls = listingService.getListing(id);
        String list = "<!DOCTYPE html>\n"
                + "<html lang=\"en\">\n"
                + "  <head>\n"
                + "    <meta charset=\"utf-8\">\n"
                + "    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n"
                + "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n"
                + "    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->\n"
                + "    <meta name=\"description\" content=\"\">\n"
                + "    <meta name=\"author\" content=\"\">\n"
                + "    <link rel=\"icon\" href=\"../../favicon.ico\">\n"
                + "\n"
                + "    <title></title>\n"
                + "\n"
                + "    <!-- Bootstrap core CSS -->\n"
                + "    <link href=\"http://localhost:8080/css/bootstrap.min.css\" rel=\"stylesheet\">\n"
                + "\n<link href=\"http://localhost:8080/css/jumbotron-narrow.css\" rel=\"stylesheet\">\n"
                + "    <!-- Custom styles for this template -->\n"
                + "    <link href=\"http://localhost:8080/css/blog.css\" rel=\"stylesheet\">\n"
                + "  </head>\n"
                + "\n"
                + "  <body>\n"
                + "\n"
                + "\n"
                + "    <div class=\"container\">\n"
                + "\n<div class=\"header clearfix\">\n"
                + "                <nav>\n"
                + "                    <ul class=\"nav nav-pills pull-right\">\n"
                + "                        <li role=\"presentation\" class=\"\"><a href=\"http://localhost:8080/sad/admin/listing\">Home</a></li>\n"
                + "                    </ul>\n"
                + "                </nav>\n"
                + "            </div>"
                + "      <div class=\"blog-header\">\n";
        if (ls.getEndDate().equalsIgnoreCase("Still available")) {
            list += "  <a id =\"add\" type=\"button\" class=\"btn btn-default\" href='http://localhost:8080/sad/admin/listing/update?id=" + id + "'>Edit</a><br>";
        } else {
            list += "  <form action='http://localhost:8080/sad/admin/listing/activate?id=" + id + "' method=\"post\"><button type=\"delete\" id =\"delete\" value=\"delete\" class=\"btn btn-default\">Reactivate</button>\n"
                    + "                   <input type=\"hidden\" name=\"submit\" value='terminate'/></form>\n";
        }
        list += "        <h2 class=\"blog-title\">" + ls.getTitle() + "</h1>\n"
                + "        <p class=\"lead blog-description\">" + ls.getStartDate().toString() + " by " + ls.getAdvertiser().getFirstName() + " " + ls.getAdvertiser().getLastName() + "</a></p>\n"
                + "      </div>\n"
                + "\n"
                + "      <div class=\"row\">\n"
                + "\n"
                + "        <div class=\"col-sm-8 blog-main\">\n"
                + "\n"
                + "          <div class=\"blog-post\">\n"
                + "<p>" + ls.getDesc() + "</p>"
                + " <p>Website: " + "<a href='" + ls.getLink() + "'>" + ls.getLink()
                + "      </div>" + "<div><form action='http://localhost:8080/sad/admin/listing/delete?id=" + id + "' method=\"post\">  <button type=\"delete\" id =\"delete\" value=\"delete\" class=\"btn btn-danger\">Delete</button>\n"
                + "                    </form>"
                + "    </div><!-- /.container -->\n"
                + "  </body>\n"
                + "</html>";

        return list;
    }
    
    
    
    

    @POST
    @Path("/addListing")
    @Produces(MediaType.TEXT_HTML)
    public String addListing(@FormParam("profileName") String profile,
            @FormParam("firstName") String first,
            @FormParam("lastName") String last,
            @FormParam("email") String email,
            @FormParam("facebook") String facebook,
            @FormParam("twitter") String twitter,
            @FormParam("phone") long phone) throws FileNotFoundException, ParseException {
            categoryService = new CategoryService();
            String form = "<form action=\"http://localhost:8080/sad/admin/listing\" method=\"post\">\n"
                    + "                <div class=\"form-group\">\n"
                    + "                    <h1>Create New Listing</h1><br>\n"
                    + "                    <label>Title:</label>\n"
                    + "                    <input class=\"form-control\" type=\"text\" name=\"title\" /><br>\n"
                    + "                    <label>Price:</label>\n"
                    + "                    <input class=\"form-control\" type=\"text\" name=\"price\" /><br>\n"
                    + "                    <label>Description:</label>\n"
                    + "                    <textarea class=\"form-control\" name=\"description\" /></textarea><br>\n"
                    + "                    <label>Link:</label>\n"
                    + "                    <input class=\"form-control\" type=\"text\" name=\"link\" /><br>\n"
                    + "                    <label>Category:</label>\n";
            if (categoryService.getAllCategories().isEmpty()) {
                form += "<input multiple class=\"form-control\" name=\"category\" placeholder=\"No categories! Create new one.\"><br>";
            } else {
                form += "<select multiple class=\"form-control\" name=\"category\">\n";
                for (Category category : categoryService.getAllCategories()) {
                    form += "<option>" + category.getTitle() + "</option>";
                }
                form += "       </select>\n<br>"
                        + "                    <label>Or create new Category:</label>\n"
                        + "                    <input class=\"form-control\" type=\"text\" name=\"category\" /><br>\n";
            }
            form += "                    <label>Feature:</label>\n"
                    + "                    <select multiple class=\"form-control\" name=\"feature\">\n"
                    + "                        <option>Regular</option>\n"
                    + "                        <option>Category-Featured</option>\n"
                    + "                        <option>Home-Page-Featured</option>\n"
                    + "                    </select>\n"
                    + "                    \n"
                    + "\n"
                    + "                    <br><br>\n"
                    + "                    <button type=\"submit\" class=\"btn btn-default\">Submit</button>\n"
                    + "                    <input type=\"hidden\" name=\"submitted\" />\n"
                    + "                    <input class=\"form-control\" type=\"hidden\" type=\"text\" name=\"profileName\" value=\"" + profile + "\"/><br>\n"
                    + "                    <input class=\"form-control\"  type=\"hidden\" type=\"text\" name=\"firstName\" value=\"" + first + "\"/><br>\n"
                    + "                    <input class=\"form-control\" type=\"hidden\" type=\"text\" name=\"lastName\" value=\"" + last + "\"/><br>\n"
                    + "                    <input class=\"form-control\" type=\"hidden\" type=\"email\" name=\"email\" value=\"" + email + "\"/><br>\n"
                    + "                    <input class=\"form-control\" type=\"hidden\" type=\"phone\" name=\"phone\" value=\"" + phone + "\"/><br>\n"
                    + "                    <input class=\"form-control\" type=\"hidden\" type=\"text\" name=\"facebook\" value=\"" + facebook + "\"/><br>\n"
                    + "                    <input class=\"form-control\" type=\"hidden\" type=\"text\" name=\"twitter\" value=\"" + twitter + "\"/><br>\n"
                    + "                </div>\n"
                    + "            </form>\n"
                    + "\n"
                    + "        </div>\n"
                    + "</html>";
            return html + form;
        
    }

    @GET
    @Path("/update")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_HTML)
    public String updateListing(@QueryParam("id") long id) throws FileNotFoundException, ParseException {
        listingService = new ListingService();
        Listing listing = listingService.getListing(id);
        if (!listing.getEndDate().equalsIgnoreCase("still available")) {
            
            return "<!DOCTYPE html>\n"
                    + "<html>\n"
                    + "<head>\n"
                    + "   <!-- HTML meta refresh URL redirection -->\n"
                    + "   <meta http-equiv=\"refresh\"\n"
                    + "   content=\"0; url=http://localhost:8080/sad/admin/listing\">"
                    + "</head>\n"
                    + "</html>";

        }

        String form = "<form action='http://localhost:8080/sad/admin/listing/update?id=" + id + "' method=\"post\">\n"
                + "                <div class=\"form-group\">\n"
                + "                    <h1>Update " + listing.getTitle() + "</h1><br>\n"
                + "                    <label>Title:</label>\n"
                + "                    <input class=\"form-control\" type=\"text\" name=\"title\" value='" + listing.getTitle() + "'/><br>\n"
                + "                    <label>Price:</label>\n"
                + "                    <input class=\"form-control\" type=\"text\" name=\"price\" value='" + listing.getPrice() + "' /><br>\n"
                + "                    <label>Description:</label>\n"
                + "                    <textarea class=\"form-control\" name=\"description\" />" + listing.getDesc() + "</textarea><br>\n"
                + "                    <label>Link:</label>\n"
                + "                    <input class=\"form-control\" type=\"text\" name=\"link\" value='" + listing.getLink() + "' /><br>\n"
                + "                    <label>Feature:</label>\n"
                + "                    <select multiple class=\"form-control\" name=\"feature\" value='" + listing.getFeature() + "'>\n"
                + "                        <option>Regular</option>\n"
                + "                        <option>Category-Featured</option>\n"
                + "                        <option>Home-Page-Featured</option>\n"
                + "                    </select>\n"
                + "                    <br><br>\n"
                + "                    <button type=\"submit\" name=\"submit\" value='edit' class=\"btn btn-default\">Update</button>\n"
                + "                    <input type=\"hidden\" name=\"submit\" value='edit'/></form>\n"
                + "                    <br></div><form action='http://localhost:8080/sad/admin/listing/terminate?id=" + id + "' method=\"post\"><button type=\"submit\" name=\"submit\" value='terminate' class=\"btn btn-danger\">Terminate</button>\n"
                + "                   <input type=\"hidden\" name=\"submit\" value='terminate'/></form>\n";

        return html + form;
    }

    @POST
    @Path("/update")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_HTML)
    public String update(@QueryParam("id") long id,
            @FormParam("title") String title,
            @FormParam("price") double price,
            @FormParam("description") String description,
            @FormParam("link") String link,
            @FormParam("feature") String feature,
            @FormParam("submit") String type
    ) throws FileNotFoundException, ParseException {

        listingService = new ListingService();
        Listing listing = listingService.getListing(id);
        listing.setDesc(description);
        listing.setFeature(feature);
        listing.setPrice(price);
        listing.setLink(link);
        listing.setTitle(title);
        listing.setId(id);
        listingService.updateListing(listing);
        
        return "<!DOCTYPE html>\n"
                + "<html>\n"
                + "<head>\n"
                + "   <!-- HTML meta refresh URL redirection -->\n"
                + "   <meta http-equiv=\"refresh\"\n"
                + "   content=\"0; url=http://localhost:8080/sad/admin/listing\">"
                + "</head>\n"
                + "</html>";
    }

    @POST
    @Path("/activate")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_HTML)
    public String terminate(@QueryParam("id") long id
    ) throws FileNotFoundException, ParseException {

        listingService = new ListingService();
        Listing listing = listingService.getListing(id);
        listing.setEndDate("Still Available");
        listingService.updateListing(listing);
        logged = true;

        return "<!DOCTYPE html>\n"
                + "<html>\n"
                + "<head>\n"
                + "   <!-- HTML meta refresh URL redirection -->\n"
                + "   <meta http-equiv=\"refresh\"\n"
                + "   content=\"0; url=http://localhost:8080/sad/admin/listing\">"
                + "</head>\n"
                + "</html>";
    }

    @POST
    @Path("/terminate")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_HTML)
    public String activate(@QueryParam("id") long id
    ) throws FileNotFoundException, ParseException {

        listingService = new ListingService();
        Listing listing = listingService.getListing(id);
        listing.setEndDate(new Date().toString());
        listingService.updateListing(listing);
        

        return "<!DOCTYPE html>\n"
                + "<html>\n"
                + "<head>\n"
                + "   <!-- HTML meta refresh URL redirection -->\n"
                + "   <meta http-equiv=\"refresh\"\n"
                + "   content=\"0; url=http://localhost:8080/sad/admin/listing\">"
                + "</head>\n"
                + "</html>";
    }

    @POST
    @Path("/delete")
    @Produces(MediaType.TEXT_HTML)
    public String deleteListing(@QueryParam("id") long id) throws FileNotFoundException, ParseException {
        listingService = new ListingService();
        listingService.removeListing(id);
        
        return "<!DOCTYPE html>\n"
                + "<html>\n"
                + "<head>\n"
                + "   <!-- HTML meta refresh URL redirection -->\n"
                + "   <meta http-equiv=\"refresh\"\n"
                + "   content=\"0; url=http://localhost:8080/sad/admin/listing\">"
                + "</head>\n"
                + "</html>";
    }

    

    @GET
    @Path("/advertiserInfo")
    @Produces(MediaType.TEXT_HTML)
    public String addAdvertiser() throws FileNotFoundException, ParseException {
        
            String form = "<form action=\"http://localhost:8080/sad/admin/listing/addListing\" method=\"post\">\n"
                    + "                <div class=\"form-group\">\n"
                    + "                    <h1>Advertiser Information</h1><br>\n"
                    + "                    <label>Profile Name:</label>\n"
                    + "                    <input class=\"form-control\" type=\"text\" name=\"profileName\" /><br>\n"
                    + "                    <label>First Name:</label>\n"
                    + "                    <input class=\"form-control\" type=\"text\" name=\"firstName\" /><br>\n"
                    + "                    <label>Last Name:</label>\n"
                    + "                    <input class=\"form-control\" type=\"text\" name=\"lastName\" /><br>\n"
                    + "                    <label>Email:</label>\n"
                    + "                    <input class=\"form-control\" type=\"email\" name=\"email\" /><br>\n"
                    + "                    <label>Phone Number:</label>\n"
                    + "                    <input class=\"form-control\" type=\"phone\" name=\"phone\" /><br>\n"
                    + "                    <label>Facebook Name:</label>\n"
                    + "                    <input class=\"form-control\" type=\"text\" name=\"facebook\" /><br>\n"
                    + "                    <label>Twitter Name:</label>\n"
                    + "                    <input class=\"form-control\" type=\"text\" name=\"twitter\" /><br>\n"
                    + "                    <br><br>\n"
                    + "                    <button type=\"submit\" class=\"btn btn-default\">Next</button>\n"
                    + "                    <input type=\"hidden\" name=\"submitted\" />\n"
                    + "                </div>\n"
                    + "            </form>\n"
                    + "\n"
                    + "        </div>\n"
                    + "</html>";
            return html + form;
        
    }
    
    
    
 
    
       
    
}
