/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resources;

import java.io.FileNotFoundException;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import model.Advertiser;
import model.Category;
import model.Listing;
import org.json.simple.parser.ParseException;
import static resources.AdminListingResource.logged;
import service.AdvertiserService;
import service.CategoryService;
import service.ListingService;

/**
 *
 * @author mousa
 */
@Path("/admin/advertiser")
public class AdvertiserResource {

    StringBuilder html = new StringBuilder("<!DOCTYPE html>\n"
            + "<html>\n"
            + "    <head>\n"
            + "        <meta charset=\"utf-8\">\n"
            + "        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n"
            + "        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n"
            + "        <!-- Bootstrap core CSS -->\n"
            + "        <link href=\"http://localhost:8080/css/bootstrap.min.css\" rel=\"stylesheet\">\n"
            + "\n"
            + "        <!-- Custom styles for this template -->\n"
            + "        <link href=\"http://localhost:8080/css/jumbotron-narrow.css\" rel=\"stylesheet\">\n"
            + "        <title>Special Advertising Directory</title>\n"
            + "    </head>\n"
            + "\n"
            + "    <body>\n"
            + "        <div class=\"container\">\n"
            + "            <div class=\"header clearfix\">\n"
            + "                <nav>\n"
            + "                    <ul class=\"nav nav-pills pull-right\">\n"
            + " <li role=\"presentation\" class=\"\"><a href=\"http://localhost:8080/sad/admin/advertiser\">Home</a></li>\n"
            + "                    </ul>\n"
            + "                </nav>\n"
            + "            </div>"
            + "");

    AdvertiserService advertiserService;

    @GET
    @Produces(MediaType.TEXT_HTML)
    public String getAdvertisers() throws FileNotFoundException, ParseException {
        advertiserService = new AdvertiserService();

        html.append("<div> <a id =\"add\" type=\"button\" class=\"btn btn-danger\" href=\"http://localhost:8080/sad/admin/advertiser/addAdvertiser\">+Add Advertiser</a></div>"
                + "<h1>Categories</h1><br><br>");
        List<Advertiser> adv = advertiserService.getAllAdvertisers();
        if (!adv.isEmpty()) {
            html.append("<table class=\"table\"><thead>\n"
                    + "        <tr>\n"
                    + "            <th>Prfile</th>\n"
                    + "            <th>Name</th>\n"
                    + "            <th>Email</th>\n"
                    + "            <th>Phone</th>\n"
                    + "        </tr>\n"
                    + "        </thead>\n"
                    + "        <tbody>");

            for (Advertiser advertiser : adv) {

                html.append("<tr><th><a href='http://localhost:8080/sad/admin/advertiser/update?id=" + advertiser.getId() + "'>" + advertiser.getProfileName() + "</th><th>" + advertiser.getFirstName() + "  " + advertiser.getLastName() + "</th><th>" + advertiser.getEmail() + "</th><th>" + advertiser.getPhone() + "</th></tr></thead>");

                html.append("</tbody>");
            }
            html.append("</tbody></table></div></html>");
        } else {
            html.append("<p>No Advertisers Found!</p>");
        }
        return html.toString();
    }

    @GET
    @Path("/{advertiserId}")
    @Produces(MediaType.TEXT_HTML)
    public String getCategory(@PathParam("advertiserId") long id) throws FileNotFoundException, ParseException {
        advertiserService = new AdvertiserService();
        Advertiser advertiser = advertiserService.getAdvertiser(id);
        html.append("<br><h1>" + advertiser.getFirstName() + " " + advertiser.getLastName() + "</h1><br><br><table class=\"table\"><thead>\n"
                + "        <tr>\n"
                + "            <th>Title</th>\n"
                + "            <th>Feature</th>\n"
                + "            <th>Price</th>\n"
                + "        </tr>\n"
                + "        </thead>\n"
                + "        <tbody>");
        for (Listing listing : advertiser.getListings()) {
            html.append("<tr><th><a href='http://localhost:8080/sad/admin/listing" + listing.getId() + "'>" + listing.getTitle() + "</th><th>" + listing.getFeature() + "</th><th>" + listing.getPrice() + "</th></tr>");
        }
        return html.toString();
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_HTML)
    public String add(@FormParam("profileName") String profile,
            @FormParam("firstName") String first,
            @FormParam("lastName") String last,
            @FormParam("email") String email,
            @FormParam("facebook") String facebook,
            @FormParam("twitter") String twitter,
            @FormParam("phone") long phone) throws FileNotFoundException, ParseException {
        advertiserService = new AdvertiserService();
        Advertiser adver = new Advertiser(profile);
        adver.setEmail(email);
        adver.setFirstName(first);
        adver.setLastName(last);
        adver.setPhone(phone);
        adver.setFacebook(facebook);
        adver.setTwitter(twitter);
        return "<!DOCTYPE html>\n"
                + "<html>\n"
                + "<head>\n"
                + "   <!-- HTML meta refresh URL redirection -->\n"
                + "   <meta http-equiv=\"refresh\"\n"
                + "   content=\"0; url=http://localhost:8080/sad/admin/advertiser\">"
                + "</head>\n"
                + "</html>";
    }

    @GET
    @Path("/addAdvertiser")
    @Produces(MediaType.TEXT_HTML)
    public String addAdvertiser() throws FileNotFoundException, ParseException {

        String form = "<form action=\"http://localhost:8080/sad/admin/advertiser/\" method=\"post\">\n"
                + "                <div class=\"form-group\">\n"
                + "                    <h1>Advertiser Information</h1><br>\n"
                + "                    <label>Profile Name:</label>\n"
                + "                    <input class=\"form-control\" type=\"text\" name=\"profileName\" /><br>\n"
                + "                    <label>First Name:</label>\n"
                + "                    <input class=\"form-control\" type=\"text\" name=\"firstName\" /><br>\n"
                + "                    <label>Last Name:</label>\n"
                + "                    <input class=\"form-control\" type=\"text\" name=\"lastName\" /><br>\n"
                + "                    <label>Email:</label>\n"
                + "                    <input class=\"form-control\" type=\"email\" name=\"email\" /><br>\n"
                + "                    <label>Phone Number:</label>\n"
                + "                    <input class=\"form-control\" type=\"phone\" name=\"phone\" /><br>\n"
                + "                    <label>Facebook Name:</label>\n"
                + "                    <input class=\"form-control\" type=\"text\" name=\"facebook\" /><br>\n"
                + "                    <label>Twitter Name:</label>\n"
                + "                    <input class=\"form-control\" type=\"text\" name=\"twitter\" /><br>\n"
                + "                    <br><br>\n"
                + "                    <button type=\"submit\" class=\"btn btn-default\">Next</button>\n"
                + "                    <input type=\"hidden\" name=\"submitted\" />\n"
                + "                </div>\n"
                + "            </form>\n"
                + "\n"
                + "        </div>\n"
                + "</html>";
        return html + form;
    }
    
    
    @POST
    @Path("/update")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_HTML)
    public String updateAdvertiser(@QueryParam("id") long id,
            @FormParam("profile") String profile,
            @FormParam("first") String first,
            @FormParam("last") String last,
            @FormParam("email") String email,
            @FormParam("phone") long phone
    ) throws FileNotFoundException, ParseException {

        advertiserService = new AdvertiserService();
        Advertiser adver = advertiserService.getAdvertiser(id);
        adver.setEmail(email);
        adver.setLastName(last);
        adver.setFirstName(first);
        adver.setPhone(phone);
        adver.setProfileName(profile);
        advertiserService.updateAdvertiser(adver);
        
        return "<!DOCTYPE html>\n"
                + "<html>\n"
                + "<head>\n"
                + "   <!-- HTML meta refresh URL redirection -->\n"
                + "   <meta http-equiv=\"refresh\"\n"
                + "   content=\"0; url=http://localhost:8080/sad/admin/advertiser\">"
                + "</head>\n"
                + "</html>";
    }
    
    
    @GET
    @Path("/update")
    @Produces(MediaType.TEXT_HTML)
    public String editAdvertiser(@QueryParam("id") long id) throws FileNotFoundException, ParseException {
    advertiserService = new AdvertiserService();
    Advertiser adv = advertiserService.getAdvertiser(id);
        String form = "<form action=\"http://localhost:8080/sad/admin/advertiser/update?id=" + id +"\" method=\"post\">\n"
                + "                <div class=\"form-group\">\n"
                + "                    <h1>Advertiser Information</h1><br>\n"
                + "                    <label>Profile Name:</label>\n"
                + "                    <input class=\"form-control\" type=\"text\" name=\"profileName\" value='"+adv.getProfileName()+"' /><br>\n"
                + "                    <label>First Name:</label>\n"
                + "                    <input class=\"form-control\" type=\"text\" name=\"firstName\" value='"+adv.getFirstName()+"' /><br>\n"
                + "                    <label>Last Name:</label>\n"
                + "                    <input class=\"form-control\" type=\"text\" name=\"lastName\" value='"+adv.getLastName()+"' /><br>\n"
                + "                    <label>Email:</label>\n"
                + "                    <input class=\"form-control\" type=\"email\" name=\"email\" value='"+adv.getEmail()+"' /><br>\n"
                + "                    <label>Phone Number:</label>\n"
                + "                    <input class=\"form-control\" type=\"phone\" name=\"phone\" value='"+adv.getPhone()+"' /><br>\n"
                + "                    <br><br>\n"
                + "                    <button type=\"submit\" class=\"btn btn-default\">Next</button>\n"
                + "                    <input type=\"hidden\" name=\"submitted\" />\n"
                + "                </div>\n"
                + "            </form>\n"
                + "\n"
                + "        </div>\n"
                + "</html>";
        return html + form;
    }

}


