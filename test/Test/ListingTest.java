/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import model.Advertiser;
import model.Category;
import model.Listing;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author mousa
 */
public class ListingTest {
    Listing l1 = new Listing("project1", 33.3, "my project is great", "www.facebook.com", new Advertiser("mhana1"));
    Listing l2 = new Listing("project2", 44, "my project is ...", "www.twitter.com", new Advertiser("mars12"));
    List<Category> cat = new ArrayList<>();
   
    public ListingTest() {
        l1.setCategories(cat);
    }
    
    @Test
    public void testGetTitle(){
        assertEquals("project1", l1.getTitle());        
    }
    
    @Test
    public void testGetPrice(){
        assertEquals(44, l2.getPrice(),1);
    }
    
    @Test 
    public void testGetDescription(){
        assertEquals("my project is great", l1.getDesc());
    }
    
    @Test
    public void testGetLink(){
        assertEquals("www.facebook.com", l1.getLink());
    }
    
    @Test
    public void testGetAdvertiser(){
        assertEquals("mhana1", l1.getAdvertiser().getProfileName());        
    }
    
    @Test
    public void testGetFeature(){
        assertEquals("Regular", l2.getFeature());
        
    }
    
    @Test
    public void testGetFinished(){
        assertEquals("Still Available", l2.getEndDate());
    }
       
    
    @Test
    public void testSetCategories(){
        List<Category> categories = new ArrayList<>();
        categories.add(new Category("Mossa"));
        l1.setCategories(categories);
        assertEquals(categories, l1.getCategories());
    }
    
    @Test
    public void testAddCategory(){
        Category category = new Category("information technology");
        cat.add(category);
        l1.setCategories(cat);
        assertEquals(true, l1.getCategories().contains(category));
    }
    
    
    @Test
    public void testSetKeywords(){
        List<String> keywords = new ArrayList<>();
        keywords.add("Mossa");
        l1.setKeywords(keywords);
        assertEquals(keywords, l1.getKeywords());
    }
    
    @Test
    public void testGetKeywords(){
        List<String> keywords = new ArrayList<>();
        keywords.add("Mossa");
        l1.setKeywords(keywords);
        assertEquals(true, l1.getKeywords().contains("Mossa"));
    }
    
    @Test
    public void testSetAdvertiser(){
        Advertiser adver = new Advertiser("mhana1");
        l2.setAdvertiser(adver);
        assertEquals("mhana1", l2.getAdvertiser().getProfileName());
    }
    
    @Test
    public void testSetFeature(){
        l1.setFeature("category-featured");
        assertEquals("category-featured", l1.getFeature());
    }
    
    @Test
    public void setTitle(){
        l1.setTitle("you're awsome");
        assertEquals("you're awsome", l1.getTitle());
    }
    
    @Test
    public void setDescription(){
        l2.setDesc("Great!");
        assertEquals("Great!", l2.getDesc());
    }
    
    @Test
    public void testSetLink(){
        l1.setLink("sad/directory.com");
        assertEquals("sad/directory.com", l1.getLink());
    }
    
    @Test
    public void testSetPrice(){
        l2.setPrice(50);
        assertEquals(50, l2.getPrice(), 1);
    }
    
    @Test
    public void testSetEnd(){
        Date now = new Date();
        l2.setEndDate(now.toString());
        assertEquals(now.toString(), l2.getEndDate());
    }
    
    @Test
    public void testReplace(){
        Listing temp = l1;
        temp =  temp.replace(l2);
        assertEquals(l2.getTitle(), temp.getTitle());
        assertEquals("my project is ...", temp.getDesc());
        assertEquals(44,temp.getPrice(),1);
        assertEquals(l2.getLink(), temp.getLink());
    }
    
    
    
    
   
}
