public class Fly extends Creature 
implements Flyer{

    public Fly(String name) {
        super(name);
    }

    public void eat(Thing aThing){
        if (Creature.class.isInstance(aThing))
            System.out.println(super.getName() + " " + super.getClass().getSimpleName() + " won't eat a " + aThing);
        
        else
           super.hasBeenEaten = aThing.getName();
    }
    
    public void move(){
        fly();
    }

    public void fly() {
       System.out.println(super.getName() + " " + super.getClass().getSimpleName() +" is buzzing around in flight");
    }
    
    
}

