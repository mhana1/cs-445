/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package service;

import database.AdvertiserParser;
import database.JsonParser;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import model.Advertiser;
import model.Category;
import model.Listing;
import org.json.simple.parser.ParseException;

/**
 *
 * @author mousa
 */
public class AdvertiserService {
    
    AdvertiserParser parser =new AdvertiserParser();
    private Map<Long, Advertiser> advertisers = new ListingService().getAdvertisers();
    private LinkedList<Advertiser> ls = new LinkedList<>();
    
    public AdvertiserService()throws FileNotFoundException, ParseException{
        for (Map.Entry<Long, Advertiser> e : parser.getAdvertisers().entrySet()) {
            if (!advertisers.containsKey(e.getKey())) {
                advertisers.put(e.getKey(), e.getValue());
            }
        }
        for (Map.Entry<Long, Advertiser> e : advertisers.entrySet()) {
            ls.add(e.getValue());
        }
    }
    
    public List<Advertiser> getAllAdvertisers(){        
        return new ArrayList<Advertiser>(advertisers.values());
    }
    
    public Advertiser getAdvertiser(long id){
        return advertisers.get(id);
    }
    
   
    
    public void updateAdvertiser(Advertiser adver) {      
        int index = ls.indexOf(adver);
        ls.get(index).replace(adver);
        parser.writeToJson(ls);

    }
    
    
    public Advertiser addCategory(Advertiser advertiser) {
        if (!advertisers.containsKey(advertiser.getId())) {
            advertisers.put(advertiser.getId(), advertiser);
            ls.add(advertiser);
            parser.writeToJson(ls);
        }
        return advertiser;
    }
    
    
    
    public List<Advertiser> searchAdvertiser(String keyword){
        //long phone = Long.valueOf(keyword).longValue();
        List<Advertiser> adList = new ArrayList<>();
        Advertiser advertiser;
        for(Advertiser adv: advertisers.values()){
            for(Listing lis : adv.getListings()){
                if(lis.getTitle().equalsIgnoreCase(keyword)){
                    adList.add(adv);
                }
            }
            if(adv.getEmail().equalsIgnoreCase(keyword) || adv.getLastName().equalsIgnoreCase(keyword) || String.valueOf(adv.getPhone()).equals(keyword)){
                adList.add(adv);
            }
            
            if(!adv.getFacebook().equals("")){
            if (adv.getFacebook().equalsIgnoreCase(keyword)){
                adList.add(adv);
            }
            }
            
            if(!adv.getTwitter().equals("")){
            if (adv.getTwitter().equalsIgnoreCase(keyword)){
                adList.add(adv);
            }
            }
        }
        return adList;
    }
    
    
}
