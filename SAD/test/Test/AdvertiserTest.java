/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Test;

import java.util.ArrayList;
import java.util.List;
import model.Advertiser;
import model.Listing;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author mousa
 */
public class AdvertiserTest {
    Advertiser adv1 = new Advertiser("mhana1");
    Advertiser adv2 = new Advertiser("mars94");
    List<Listing> listings = new ArrayList<>();
    
    public AdvertiserTest() {
        adv1.setListings(listings);
    }
    
     @Test
    public void testGetProfile(){        
        assertEquals("mhana1", adv1.getProfileName());        
    }
    
    @Test
    public void testFirst(){
        adv1.setFirstName("Mossa");
        assertEquals("Mossa", adv1.getFirstName());
    }
    
    @Test 
    public void testLast(){
        adv1.setLastName("Hana");
        assertEquals("Hana", adv1.getLastName());
    }
    
    @Test
    public void testEmail(){
        adv1.setEmail("mhana1@hawk.iit.edu");
        assertEquals("mhana1@hawk.iit.edu", adv1.getEmail());
    }
    
    @Test
    public void testPhone(){
        adv1.setPhone(773742570);
        assertEquals(773742570, adv1.getPhone(), 1);        
    }
    
    @Test
    public void testFacebook(){
        adv1.setFacebook("Mousa Hana");
        assertEquals("Mousa Hana", adv1.getFacebook());
        
    }
    
    @Test
    public void testTwitter(){
        adv1.setTwitter("MossaHana");
        assertEquals("MossaHana", adv1.getTwitter());
        
    }
    
    
    @Test
    public void testGetListings(){
        assertSame(listings, adv1.getListings());
        
    }
    
    @Test
    public void testSetListings(){
        List<Listing> newList = new ArrayList<>();
        adv1.setListings(newList);
        assertSame(newList, adv1.getListings());
        
    }
    
    
    @Test
    public void testAddListing(){
        Listing listing = new Listing("information technology");
        adv1.addListing(listing);
        assertEquals(true, adv1.getListings().contains(listing));
    }
    
}
