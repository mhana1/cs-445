
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author mousa
 */
public class MyTests {

    public MyTests() {
    }
    
    @Test
    public void testClasses(){
        Thing thing1 = new Thing("ball");
        assertEquals("ball", thing1.getName());
        Ant ant1 = new Ant("nana");
        assertEquals("nana", ant1.getName());
        Bat bat1 = new Bat("bobe");
        assertEquals("bobe", bat1.getName());
        Fly fly1 = new Fly("flemo");
        assertEquals("flemo", fly1.getName());
        Tiger tiger1 = new Tiger("taygora");
        assertEquals("taygora", tiger1.getName());
    }
    
    @Test
    public void testGettingClassName(){
        Ant ant1 = new Ant("nana");
        Thing thing1 = new Thing("ball");
        Fly fly1 = new Fly("flemo");
        assertEquals("Ant", ant1.getClass().getSimpleName());
        assertEquals("Thing", thing1.getClass().getSimpleName());
        assertEquals("Fly", fly1.getClass().getSimpleName());
    }
    
    @Test
    public void testPrintingCreature(){
        Creature tiger = new Tiger("tagoro");
        assertEquals("tagoro Tiger", tiger.toString());
        
    }

    @Test
    public void flysCanNotEatCreatures() {
        Thing thing1 = new Thing("banana");
        Thing thing2 = new Thing("juice");
        Creature animal = new Tiger("foxie");
        Fly fly1 = new Fly("wzz");
        Fly fly2 = new Fly("wzz2");

        fly1.eat(animal);
        assertEquals(null, fly1.hasBeenEaten);
        fly2.eat(thing2);
        assertEquals("juice", fly2.hasBeenEaten);
    }

    @Test
    public void flysCanFly() {
        Creature fly1 = new Fly("wzz");
        fly1.move();
    }

    @Test
    public void antsCanMove() {
        Ant ant1 = new Ant("ntoot");
        ant1.move();
    }
    
    @Test
    public void antsCanEatAnything(){
        Ant ant1 = new Ant("toti");
        Thing thing1 = new Thing("bread");
        ant1.eat(thing1);
    }

    @Test
    public void tigersCanMove() {
        Creature tiger1 = new Tiger("togo");
        tiger1.move();
    }
    
    @Test
    public void tigersCanEatAnything() {
        Tiger tiger1 = new Tiger("togo");
        Tiger tiger2 = new Tiger("paco");
        tiger1.eat(tiger2);
    }
    
    @Test
    public void batsCanFly() {
        Bat bat1 = new Bat("batman");
        bat1.move();
    }

    @Test
    public void batEatsOnlyCreatures() {
        Bat bat1 = new Bat("tabo");
        Fly fly1 = new Fly("loli");
        Thing thing1 = new Thing("ball");        
        bat1.eat(thing1);
                
        bat1.eat(fly1);
        assertEquals("loli", bat1.hasBeenEaten);  
        
    }
}