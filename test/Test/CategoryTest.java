/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Test;

import java.util.ArrayList;
import java.util.List;
import model.Advertiser;
import model.Category;
import model.Listing;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author mousa
 */
public class CategoryTest {
    Category cat1 = new Category("Art and Entairtainment");
        List<Listing> listings = new ArrayList<>();
    
    
    public CategoryTest() {
        listings.add(new Listing("project", 33.3, "my project is great", "www.facebook.com", new Advertiser("mhana1")));
        cat1.setListings(listings);
    }
     
    
    @Test
    public void testGettingClassName(){
        
        assertEquals("Category", cat1.getClass().getSimpleName());
    }
    
    @Test
    public void testGetTitle(){
        
        assertEquals("Art and Entairtainment", cat1.getTitle());
        
    }
    
    @Test
    public void testGetListings(){
        assertSame(listings, cat1.getListings());
        
    }
    
    @Test
    public void testSetTitle(){
        String title = "mossa";
        cat1.setTitle(title);
        assertEquals("mossa", cat1.getTitle());
    }
    
    @Test
    public void testSetListings(){
        List<Listing> listings = new ArrayList<>();
        cat1.setListings(listings);
        assertEquals(listings, cat1.getListings());
    }
    
    @Test
    public void testSetId(){
        long id = 12;
        cat1.setId(id);
        assertEquals(id, cat1.getId());
    }
    
    @Test
    public void testAddListing(){
        Listing listing = new Listing("information technology");
        cat1.addListing(listing);
        assertEquals(true, cat1.getListings().contains(listing));
    }
    
    
    

   
}
