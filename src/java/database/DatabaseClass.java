/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package database;


import model.Advertiser;
import java.util.HashMap;
import java.util.Map;

import model.Category;
import model.Listing;


/**
 *
 * @author mousa
 */
public class DatabaseClass {
    String listingsFile = "src/java/xml/listings.xml";
    
    private static Map<Long, Listing> listings = new HashMap<>();
    private static Map<String, Category> categories = new HashMap<>();
    private static Map<String, Advertiser> advertisers = new HashMap<>();
    
       
    
    public static Map<Long,Listing> getListings(){
        return listings;
    }
    
    public static Map<String,Category> getCategories(){
        return categories;
    }
    
    public static Map<String, Advertiser> getAdvertisers(){
        return advertisers;
    }
        

  }
    
    
