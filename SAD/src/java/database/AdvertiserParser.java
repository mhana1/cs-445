/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package database;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import model.Advertiser;
import model.Category;
import model.Listing;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author mousa
 */
public class AdvertiserParser {
    private LinkedList<Advertiser> linkedList;
    private Map<Long, Advertiser> advertisers;

    JSONParser parser = new JSONParser();
    String file = "C:\\Users\\mousa\\Documents\\NetBeansProjects\\cs445\\SAD\\src\\java\\xml\\advertisers.json";

    public AdvertiserParser() throws FileNotFoundException, org.json.simple.parser.ParseException {

        advertisers = new HashMap<>();
        linkedList = new LinkedList<>();
        if (!file.isEmpty()) {
            parseJson();
        }
    }

    public Map<Long, Advertiser> getAdvertisers() {
        return advertisers;
    }

    public LinkedList<Advertiser> getLs() {
        return linkedList;
    }

    public void parseJson() throws FileNotFoundException, org.json.simple.parser.ParseException {
        try {
            Advertiser tempAdvertiser;
            Object obj = parser.parse(new FileReader(file));

            JSONArray advertiserAr = (JSONArray) obj;
            JSONObject jsonObject;
            JSONObject object;
            JSONObject object1;
            JSONObject object2;
            JSONObject object3;

            Iterator<JSONObject> i = advertiserAr.iterator();
            //System.out.println("i="+ i);
            while (i.hasNext()) {
                System.out.println("i=" + i);
                object = i.next();
                object3 = (JSONObject) object.get("advertiser");
                tempAdvertiser = new Advertiser((String) object3.get("profile"));
                jsonObject = object3;
                String profile = (String) jsonObject.get("profile");
                tempAdvertiser.setProfileName(profile);
                String first = (String) jsonObject.get("firstName");
                tempAdvertiser.setFirstName(first);
                String last = (String) jsonObject.get("lastName");
                tempAdvertiser.setLastName(last);
                String email  = (String) jsonObject.get("email");
                tempAdvertiser.setEmail(email);
                String facebook = (String) jsonObject.get("facebook");
                tempAdvertiser.setFacebook(facebook);
                String twitter = (String) jsonObject.get("twitter");
                tempAdvertiser.setFacebook(twitter);
                long id = (Long) jsonObject.get("id");
                tempAdvertiser.setId(id);
                long phone = (Long) jsonObject.get("phone");
                tempAdvertiser.setId(phone);
                JSONArray listingss = (JSONArray) jsonObject.get("listings");
                List<Listing> lists = new ArrayList<>();
                Iterator<JSONObject> iterator = listingss.iterator();
                //JSONArray catLis;
                while (iterator.hasNext()) {
                    object2 = iterator.next();
                    Listing tempListing = new Listing((String) object2.get("title"));
                    long lid = (Long) object2.get("id");
                    tempListing.setId(lid);
                    double price = (Double) object2.get("price");
                    tempListing.setPrice(price);
                    Date created = new Date((String) object2.get("created"));
                    tempListing.setStartDate(created);
                    String finished = (String) object2.get("finished");
                    tempListing.setEndDate(finished);
                    String desc = (String) object2.get("description");
                    tempListing.setDesc(desc);
                    String link = (String) object2.get("link");
                    tempListing.setLink(link);
                    String feature = (String) object2.get("feature");
                    tempListing.setFeature(feature);
                    JSONArray categories = (JSONArray) object2.get("categories");
                    List<Category> cats = new ArrayList<>();
                if (!categories.isEmpty()) {
                    Iterator<JSONObject> iter = categories.iterator();
                    while (iter.hasNext()) {
                        object1 = iter.next();
                        String ctitle = (String) object1.get("title");
                        long cid = (long) object1.get("id");
                        Category tempCategory = new Category(ctitle);
                        tempCategory.setId(cid);
                        cats.add(tempCategory);
                    }
                    tempListing.setCategories(cats);
                }
                    tempListing.setAdvertiser(tempAdvertiser);
                    tempAdvertiser.addListing(tempListing);
                }

                if (!this.advertisers.containsKey(tempAdvertiser.getId())) {
                    this.advertisers.put(tempAdvertiser.getId(), tempAdvertiser);
                    //this.categories.get(cat.getTitle()).addListing(tempListing);
                }

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void writeToJson(LinkedList<Advertiser> ls) {
        JSONArray obj4 = new JSONArray();
        while (ls.peek() != null) {
            Advertiser tempAdvertiser = ls.poll();
            JSONObject obj3 = new JSONObject();
            JSONObject obj = new JSONObject();
            JSONObject obj1;
            JSONObject obj2;
            long id = tempAdvertiser.getId();
            long phone = tempAdvertiser.getPhone();
            String profile = tempAdvertiser.getProfileName(); 
            String first = tempAdvertiser.getFirstName();
            String last = tempAdvertiser.getLastName();
            String email = tempAdvertiser.getEmail();
            String facebook = tempAdvertiser.getFacebook();
            String twitter = tempAdvertiser.getTwitter();
            
            List<Listing> listings = tempAdvertiser.getListings();
            obj.put("id", id);
            obj.put("profile", profile);
            obj.put("firstName", first);
            obj.put("lastName", last);
            obj.put("email",email);
            obj.put("phone", phone);
            obj.put("facebook", facebook);
            obj.put("twitter", twitter);
            JSONArray list = new JSONArray();
            JSONArray catsArr = new JSONArray();
            
            
            for (Listing lis : listings) {
                List<Category> cats = lis.getCategories();
                for(Category c: cats){
                    obj1 = new JSONObject();
                    obj1.put("id", c.getId());
                    obj1.put("title", c.getTitle());
                
                    catsArr.add(obj1);
                }
                
                obj2 = new JSONObject();
                obj2.put("id", lis.getId());
                obj2.put("title", lis.getTitle());
                obj2.put("advertiser", lis.getAdvertiser().getProfileName());
                obj2.put("finished", lis.getEndDate());
                obj2.put("description", lis.getDesc());
                obj2.put("link", lis.getLink());
                obj2.put("price", lis.getPrice());
                obj2.put("feature", lis.getFeature());
                obj2.put("created", lis.getStartDate().toString());                
                obj2.put("categories", catsArr);
                
                list.add(obj2);
            }
            obj.put("listings", list);
            
            obj3.put("advertiser", obj);
            obj4.add(obj3);
        }
        try {

            FileWriter f = new FileWriter(file);
            f.write(obj4.toJSONString());
            f.flush();
            f.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
