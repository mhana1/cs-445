
public abstract class Creature 
extends Thing{
    
     
    public String hasBeenEaten;
            
    public Creature(String name) {
        super(name);
    }
   
    public void eat(Thing aThing){
        System.out.println(super.getName() + " has just eaten a " + aThing);
        hasBeenEaten = aThing.getName();
    }
    
    public abstract void move();
    
    public void whatDidYouEat(){
        if (hasBeenEaten.equals(null))
           System.out.println(super.getName() + super.getClass().getSimpleName() + " has had nothing to eat!");
           
        else
            System.out.println(super.getName() + super.getClass().getSimpleName() + " has eaten a " + hasBeenEaten);
    }
}
