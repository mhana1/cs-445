/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import model.Advertiser;
import model.Listing;
import service.AdvertiserService;

/**
 *
 * @author mousa
 */

@Path("/admin/advertisers")
public class AdvertiserResource {
    
    AdvertiserService advertiserService;
    
    @GET
    @Produces(MediaType.TEXT_HTML)
    public String getAdvertisers(){
       advertiserService.getAllAdvertisers();
       return "<html>";
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_HTML)
    public String addAdvertiser(Advertiser advertiser){       
        advertiserService.addAdvertiser(advertiser);      
        return "<html>";
    }
    
    @GET
    @Path("/{advertiserName}")
    @Produces(MediaType.TEXT_HTML)
    public String getAdvertiser(@PathParam("advertiserName") String profileName){ 
        advertiserService.getAdvertiser(profileName);
        return "<html> ";
    }
    
    @PUT
    @Path("/{advertiserName}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_HTML)
    public String updateAdvertiser(@PathParam("advertiserName") String profileName,  Advertiser advertiser){
        advertiser.setProfileName(profileName);
        advertiserService.updateAdvertiser(advertiser);
        return "<html>";
    }
    
    @DELETE
    @Path("/{advertiserName}")
    @Produces(MediaType.TEXT_HTML)
    public String deleteAdvertiser(@PathParam("advertiserName") String profileName){
        advertiserService.removeAdvertiser(profileName);
        return "<html>";
    }

    Iterable<Advertiser> searchAdvertiser(String query) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
