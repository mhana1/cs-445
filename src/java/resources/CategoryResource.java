/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resources;

import java.io.FileNotFoundException;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import model.Category;
import model.Listing;
import org.json.simple.parser.ParseException;
import service.CategoryService;
import service.ListingService;

/**
 *
 * @author mousa
 */
@Path("/directory/categories")
public class CategoryResource {

    StringBuilder html = new StringBuilder("<!DOCTYPE html>\n"
            + "<html>\n"
            + "    <head>\n"
            + "        <meta charset=\"utf-8\">\n"
            + "        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n"
            + "        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n"
            + "        <!-- Bootstrap core CSS -->\n"
            + "        <link href=\"http://localhost:8080/css/bootstrap.min.css\" rel=\"stylesheet\">\n"
            + "\n"
            + "        <!-- Custom styles for this template -->\n"
            + "        <link href=\"http://localhost:8080/css/jumbotron-narrow.css\" rel=\"stylesheet\">\n"
            + "        <title>Special Advertising Directory</title>\n"
            + "    </head>\n"
            + "\n"
            + "    <body>\n"
            + "        <div class=\"container\">\n"
            + "            <div class=\"header clearfix\">\n"
            + "                <nav>\n"
            + "                    <ul class=\"nav nav-pills pull-right\">\n"
            +" <li role=\"presentation\" class=\"\"><a href=\"http://localhost:8080/sad/directory\">Home</a></li>\n"
            + "                    </ul>\n"
            + "                </nav>\n"
            + "            </div>"
            + "<div class=\"jumbotron\">");

    CategoryService categoryService ;

    @GET
    @Produces(MediaType.TEXT_HTML)
    public String getCategories() throws FileNotFoundException, ParseException {
        categoryService = new CategoryService();
        
        html.append("<br><h1>Categories</h1><br><br>");
        List<Category> cat = categoryService.getAllCategories();
        if(!cat.isEmpty()){
            html.append("<table class=\"table\"><thead>\n"
            + "        <tr>\n"
            + "            <th>Title</th>\n"
            + "            <th>Number of Listings</th>\n"
            + "        </tr>\n"
            + "        </thead>\n"
            + "        <tbody>");
        
        for (Category category : cat) {
            int number =0;
            for(Listing listing: category.getListings()){
                number++;
            }
            html.append("<tr><th><a href='http://localhost:8080/sad/directory/categories/" + category.getTitle()+"'>" + category.getTitle()  + "</th><th>"+ number+ "</th></tr></thead><tbody>");
            
            html.append("</tbody>");
        }
        html.append("</table></div></html>");
        }
        else{
            html.append("<p>No Categories Found!</p>");
        }
        return html.toString();
    }


    @GET
    @Path("/{categoryTitle}")
    @Produces(MediaType.TEXT_HTML)
    public String getCategory(@PathParam("categoryTitle") String title) throws FileNotFoundException, ParseException {
        categoryService = new CategoryService();
        Category category = categoryService.getCategory(title);
        html.append("<br><h1>" + title + "</h1><br><br><table class=\"table\"><thead>\n"
            + "        <tr>\n"
            + "            <th>Title</th>\n"
            + "            <th>Advertiser</th>\n"
            + "            <th>Price</th>\n"
            + "        </tr>\n"
            + "        </thead>\n"
            + "        <tbody>");
        for(Listing listing: category.getListings() ){
            html.append("<tr><th><a href='http://localhost:8080/sad/directory/listings/" + listing.getId() + "'>" + listing.getTitle() + "</th><th>"+ listing.getAdvertiser().getFirstName()+" " +listing.getAdvertiser().getLastName() + "</th><th>" +listing.getPrice() +"</th></tr>");
        }
        return html.toString();
    }

}
