package model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import javax.swing.ImageIcon;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Listing {

    private final DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    private static final AtomicInteger count = new AtomicInteger(0);
    private long id;
    private String title;
    private double price;
    private Date created;
    private String finished;
    private String desc;
    private String link;
    private List<Category> categories = new ArrayList<>();

    
    static final String[] FEATURES = {"Regular", "Category-Featured", "Home-Page-Featured"};
    private String feature;
    private Advertiser advertiser;
    private List<String> keywords = new ArrayList<>();

    public Listing() {

    }
    
    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    public Listing(String title, double price, String desc, String link, Advertiser adver) {
        this.id = count.incrementAndGet();
        this.title = title;
        this.price = price;
        this.desc = desc;
        this.link = link;
        this.advertiser = adver;
        this.created = new Date();
        this.finished = "Still Available";
        this.feature = FEATURES[0];
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Listing(String title) {
        super();
        this.title = title;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Date getStartDate() {
        return created;
    }

    public void setStartDate(Date startDate) {
        this.created = startDate;
    }

    public String getEndDate() {
        return finished;
    }

    public void setEndDate(String endDate) {
        this.finished = endDate;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getFeature() {
        return feature;
    }

    public void setFeature(String feature) {
        this.feature = feature;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Advertiser getAdvertiser() {
        return advertiser;
    }

    public void setAdvertiser(Advertiser advertiser) {
        this.advertiser = advertiser;
    }

    
    public Listing replace(Listing listing){
        Listing ls = new Listing(listing.getTitle(), listing.getPrice(), listing.getDesc(), listing.getLink(), listing.getAdvertiser());
        ls.setCategories(listing.getCategories());
        ls.setFeature(listing.getFeature());
        ls.setId(listing.getId());
        ls.setStartDate(listing.getStartDate());
        ls.setEndDate(listing.getEndDate());
        ls.setKeywords(listing.getKeywords());        
        return ls;
    }
    
    

}
