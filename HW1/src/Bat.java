public class Bat extends Creature
        implements Flyer {

    public Bat(String name) {
        super(name);
    }

    public void eat(Thing aThing) {
        if (Creature.class.isInstance(aThing)) 
            super.hasBeenEaten = aThing.getName();
        else if(!Creature.class.isInstance(aThing))
            System.out.println(super.getName() + " " + super.getClass().getSimpleName() + " won't eat a " + aThing);
        else
            ;
    }

    public void move() {
        fly();
    }

    public void fly() {
        System.out.println(super.getName() + " " + super.getClass().getSimpleName() + " is swooping through the dark");
    }

}