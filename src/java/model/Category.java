package model;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import javax.annotation.Generated;
import javax.persistence.GeneratedValue;

public class Category {

    private static final AtomicInteger count = new AtomicInteger(0);
    private long id;
    private String title;
    private List<Listing> listings = new ArrayList<>();

    public Category() {
        this.id = count.incrementAndGet();
    }
    
    public Category(String title) {
        this.id = count.incrementAndGet();
        this.title = title;
    }
    

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Category(String title, Listing listing) {
        super();
        this.title = title;
        listings.add(listing);
        this.id = count.incrementAndGet();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Listing> getListings() {
        return listings;
    }

    public void setListings(List<Listing> listings) {
        this.listings = listings;
    }

    public void addListing(Listing listing) {
        this.listings.add(listing);
    }

}
