/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import database.CategoryParser;
import database.JsonParser;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import model.Category;
import model.Listing;
import org.json.simple.parser.ParseException;

/**
 *
 * @author mousa
 */
public class CategoryService {

    CategoryParser parser = new CategoryParser();
    private Map<String, Category> categories = new ListingService().getCategories();
    private LinkedList<Category> ls = new LinkedList<Category>();

    public CategoryService() throws FileNotFoundException, ParseException {
        for (Entry<String, Category> e : parser.getCategories().entrySet()) {
            if (!categories.containsKey(e.getKey())) {
                categories.put(e.getKey(), e.getValue());
            }
        }
        for (Entry<String, Category> e : categories.entrySet()) {
            ls.add(e.getValue());
        }
    }

    public List<Category> getAllCategories() {
        return new ArrayList<Category>(categories.values());
    }

    public Category getCategory(String title) {
        return categories.get(title);
    }

    public Category getCategory(long id) {
        for (Entry<String, Category> e : categories.entrySet()) {
            if (e.getValue().getId() == id) {
                return e.getValue();
            }

        }
        return null;
    }

    public Category addCategory(Category category) {
        if (!categories.containsKey(category.getTitle())) {
            categories.put(category.getTitle(), category);
            ls.add(category);
            parser.writeToJson(ls);
        }
        return category;
    }

    public void updateCategory(Category category) {
        /*for (Entry<String, Category> e : categories.entrySet()) {
            if (e.getValue().getId() == category.getId()) {
                e.getValue().setTitle(category.getTitle());
                
            }
        }*/
        int index = ls.indexOf(category);
                ls.get(index).replace(category);
                parser.writeToJson(ls);

    }

    public void removeCategory(String title) {
        ls.remove(categories.get(title));
        parser.writeToJson(ls);
    }

}
