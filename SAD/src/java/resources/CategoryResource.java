/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resources;

import java.io.FileNotFoundException;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import model.Advertiser;
import model.Category;
import model.Listing;
import org.json.simple.parser.ParseException;
import static resources.AdminListingResource.logged;
import service.AdvertiserService;
import service.CategoryService;
import service.ListingService;

/**
 *
 * @author mousa
 */
@Path("/admin/category")
public class CategoryResource {

    StringBuilder html = new StringBuilder("<!DOCTYPE html>\n"
            + "<html>\n"
            + "    <head>\n"
            + "        <meta charset=\"utf-8\">\n"
            + "        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n"
            + "        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n"
            + "        <!-- Bootstrap core CSS -->\n"
            + "        <link href=\"http://localhost:8080/css/bootstrap.min.css\" rel=\"stylesheet\">\n"
            + "\n"
            + "        <!-- Custom styles for this template -->\n"
            + "        <link href=\"http://localhost:8080/css/jumbotron-narrow.css\" rel=\"stylesheet\">\n"
            + "        <title>Special Advertising Directory</title>\n"
            + "    </head>\n"
            + "\n"
            + "    <body>\n"
            + "        <div class=\"container\">\n"
            + "            <div class=\"header clearfix\">\n"
            + "                <nav>\n"
            + "                    <ul class=\"nav nav-pills pull-right\">\n"
            + " <li role=\"presentation\" class=\"\"><a href=\"http://localhost:8080/sad/admin/category\">Home</a></li>\n"
            + "                    </ul>\n"
            + "                </nav>\n"
            + "            </div>"
            + "");

    CategoryService categoryService;

    @GET
    @Produces(MediaType.TEXT_HTML)
    public String getCategories() throws FileNotFoundException, ParseException {
        categoryService = new CategoryService();

        html.append("<div> <a id =\"add\" type=\"button\" class=\"btn btn-danger\" href=\"http://localhost:8080/sad/admin/category/addCategory\">+Add Category</a></div><br>"
                + "<h1>Categories</h1><br>");
        List<Category> cat = categoryService.getAllCategories();
        if (!cat.isEmpty()) {
            html.append("<table class=\"table\"><thead>\n"
                    + "        <tr>\n"
                    + "            <th>Title</th>\n"
                    + "            <th>Number of Listings</th>\n"
                    + "        </tr>\n"
                    + "        </thead>\n"
                    + "        <tbody>");

            for (Category category : cat) {
                int number = 0;
                for (Listing listing : category.getListings()) {
                    number++;
                }
                html.append("<tr><th><a href='http://localhost:8080/sad/admin/category/update?title=" + category.getTitle() + "'>" + category.getTitle() + "</th><th>" + number + "</th></tr></thead><tbody>");

                html.append("</tbody>");
            }
            html.append("</table></div></html>");
        } else {
            html.append("<p>No Categories Found!</p>");
        }
        return html.toString();
    }

    @GET
    @Path("/{categoryTitle}")
    @Produces(MediaType.TEXT_HTML)
    public String getCategory(@PathParam("categoryTitle") String title) throws FileNotFoundException, ParseException {
        categoryService = new CategoryService();
        Category category = categoryService.getCategory(title);
        html.append("<br><h1>" + title + "</h1><br><br><table class=\"table\"><thead>\n"
                + "        <tr>\n"
                + "            <th>Title</th>\n"
                + "            <th>Advertiser</th>\n"
                + "            <th>Price</th>\n"
                + "        </tr>\n"
                + "        </thead>\n"
                + "        <tbody>");
        for (Listing listing : category.getListings()) {
            html.append("<tr><th><a href='http://localhost:8080/sad/admin/listing" + listing.getId() + "'>" + listing.getTitle() + "</th><th>" + listing.getAdvertiser().getFirstName() + " " + listing.getAdvertiser().getLastName() + "</th><th>" + listing.getPrice() + "</th></tr>");
        }
        return html.toString();
    }

    @GET
    @Path("/addCategory")
    @Produces(MediaType.TEXT_HTML)
    public String addCategory() {
        html.append("<form action=\"http://localhost:8080/sad/admin/category\" method=\"post\">\n"
                + "                <div class=\"form-group\">\n"
                + "                    <h1>Create Category</h1><br>\n"
                + "                    <label>Category Title:</label>\n"
                + "                    <input class=\"form-control\" type=\"text\" name=\"title\" /><br>\n<button type=\"submit\" class=\"btn btn-default\">Create</button>\n</form>");

        return html.toString();

    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_HTML)
    public String add(@FormParam("title") String title) throws FileNotFoundException, ParseException {
        categoryService = new CategoryService();
        Category cat = new Category(title);
        categoryService.addCategory(cat);

        return "<!DOCTYPE html>\n"
                + "<html>\n"
                + "<head>\n"
                + "   <!-- HTML meta refresh URL redirection -->\n"
                + "   <meta http-equiv=\"refresh\"\n"
                + "   content=\"0; url=http://localhost:8080/sad/admin/category\">"
                + "</head>\n"
                + "</html>";
    }
    
    @POST
    @Path("/update")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_HTML)
    public String updateCategory(@QueryParam("title") String title,
            @FormParam("title") String title2
    ) throws FileNotFoundException, ParseException {

        categoryService = new CategoryService();
        Category cat = categoryService.getCategory(title);
        cat.setTitle(title2);
        cat.setId(categoryService.getCategory(title).getId());
        categoryService.updateCategory(cat);
        logged = true;
        return "<!DOCTYPE html>\n"
                + "<html>\n"
                + "<head>\n"
                + "   <!-- HTML meta refresh URL redirection -->\n"
                + "   <meta http-equiv=\"refresh\"\n"
                + "   content=\"0; url=http://localhost:8080/sad/admin/category\">"
                + "</head>\n"
                + "</html>";
    }
    
    @GET
    @Path("/update")
    @Produces(MediaType.TEXT_HTML)
    public String editAdvertiser(@QueryParam("title") String title) throws FileNotFoundException, ParseException {
    categoryService = new CategoryService();
    Category cat = categoryService.getCategory(title);
        String form = "<form action='http://localhost:8080/sad/admin/category/delete?title=" + title + "' method=\"post\"><button type=\"delete\" id =\"delete\" value=\"delete\" class=\"btn btn-danger\">Delete</button>\n"
                    + "                   <input type=\"hidden\" name=\"submit\" value='terminate'/></form><form action=\"http://localhost:8080/sad/admin/category/update?title="+title+"\" method=\"post\">\n"
                + "                <div class=\"form-group\">\n"
                + "                    <h1>Edit Category</h1><br>\n"
                + "                    <label>Category Title:</label>\n"
                + "                    <input class=\"form-control\" type=\"text\" value='"+ title +"' name=\"title\" /><br>\n<button type=\"submit\" class=\"btn btn-default\">Edit</button>\n</form>"
                + "        </div>\n"
                + "</html>";
        return html + form;
    }
    
    @POST
    @Path("/delete")
    public String delete(@QueryParam("title") String title) throws FileNotFoundException, ParseException{
        categoryService = new CategoryService();
        categoryService.removeCategory(title);
        
        return "<!DOCTYPE html>\n"
                + "<html>\n"
                + "<head>\n"
                + "   <!-- HTML meta refresh URL redirection -->\n"
                + "   <meta http-equiv=\"refresh\"\n"
                + "   content=\"0; url=http://localhost:8080/sad/admin/category\">"
                + "</head>\n"
                + "</html>";
    }

}
