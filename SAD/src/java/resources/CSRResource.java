/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resources;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import model.Advertiser;
import model.Category;
import model.Listing;
import org.json.simple.parser.ParseException;
import static resources.AdminListingResource.logged;
import service.AdvertiserService;
import service.CategoryService;
import service.ListingService;

/**
 *
 * @author mousa
 */
@Path("/csr")
public class CSRResource {

  

    String html = "<!DOCTYPE html>\n"
            + "<html>\n"
            + "    <head>\n"
            + "        <meta charset=\"utf-8\">\n"
            + "        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n"
            + "        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n"
            + "        <!-- Bootstrap core CSS -->\n"
            + "        <link href=\"http://localhost:8080/css/bootstrap.min.css\" rel=\"stylesheet\">\n"
            + "\n"
            + "        <!-- Custom styles for this template -->\n"
            + "        <link href=\"http://localhost:8080/css/jumbotron-narrow.css\" rel=\"stylesheet\">\n"
            + "        <title>Special Advertising Directory</title>\n"
            + "    </head>\n"
            + "\n"
            + "    <body>\n"
            + "        <div class=\"container\">\n"
            + "            <div class=\"header clearfix\">\n"
            + "                <nav>\n"
            + "                    <ul class=\"nav nav-pills pull-right\">\n"
            + "                        <li role=\"presentation\" class=\"\"><a href=\"http://localhost:8080/sad/csr\">Home</a></li>\n";

    String form = "                </ul></nav></div>\n<h1>Please Login</h1><br>\n"
            + "        <form action=\"http://localhost:8080/sad/csr\" method=\"post\">\n"
            + "            <div class=\"form-group\">\n"
            + "                <label for=\"exampleInputEmail1\">Username</label>\n"
            + "                <input type=\"name\" name=\"username\" class=\"form-control\" id=\"exampleInputEmail1\" placeholder=\"Username\">\n"
            + "            </div>\n"
            + "            <div class=\"form-group\">\n"
            + "                <label for=\"exampleInputPassword1\">Password</label>\n"
            + "                <input type=\"password\" name=\"password\" class=\"form-control\" id=\"exampleInputPassword1\" placeholder=\"Password\">\n"
            + "            </div>\n"
            + "            <br>\n"
            + "            <button type=\"submit\" class=\"btn btn-default\">Submit</button>\n"
            + "           \n"
            + "        </form>\n"
            + "        </div>\n"
            + "    </body>\n"
            + "</html>";

    String search = "            </ul></nav></div>"
            + "\n"
            + "            <div class=\"jumbotron\">    <div class=\"col-left\">\n"
            + "                    <h3 class=\"sprite search-head\"><b>Search for advertiser by</h3>\n"
            + "                    <form id=\"search-form-tray\" name=\"search-form-tray\" action=\"http://localhost:8080/sad/csr/search\" method=\"post\" accept-charset=\"utf-8\">\n"
            + "                        <label>Email, Phone, Lastname, Business</label><br>\n"
            + "                        <input class=\"form-control\" type=\"text\" name=\"query\" value=\"\" id=\"searchText\" /><br>\n"
            + "                         </div>";

    String end = "               </div>\n"
            + "            <button type=\"submit\" class=\"btn btn-default\">Search</button>\n"
            + "    </div></body></html>\n";

    ListingService listingService;
    CategoryService categoryService;
    AdvertiserService advertiserService;

    @GET
    @Produces(MediaType.TEXT_HTML)
    public String login() throws FileNotFoundException, ParseException {
        
        return html + search + end;
    }

    

    @POST
    @Path("/search")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_HTML)
    public String searchTaker(@FormParam("query") String query) throws FileNotFoundException, ParseException {
        
            return "<!DOCTYPE html>\n"
                    + "<html>\n"
                    + "<head>\n"
                    + "   <!-- HTML meta refresh URL redirection -->\n"
                    + "   <meta http-equiv=\"refresh\"\n"
                    + "   content=\"0; url=http://localhost:8080/sad/csr/search?q=" + query + "\">\n"
                    + "</head>\n"
                    + "</html>";
        
    }

    @GET
    @Path("/search")
    @Produces(MediaType.TEXT_HTML)
    public String search(@QueryParam("q") String query) throws FileNotFoundException, ParseException {
        
            advertiserService = new AdvertiserService();
            List<Advertiser> ls = advertiserService.searchAdvertiser(query);
            if (!ls.isEmpty()) {
                html += "</ul></nav></div><h2 class='lefto'><b>Results for: '" + query + "'</h2><br>" + "<table class='table'><thead>"
                        + "        <tr>\n"
                        + "            <th>Profile</th>\n"
                        + "            <th>Name</th>\n"
                        + "            <th>Email</th>\n"
                        + "            <th>Phone</th>\n"
                        + "        </tr>\n"
                        + "        </thead>\n"
                        + "        <tbody>";
                for (Advertiser adv : ls) {
                    html += "<tr><th><a href='http://localhost:8080/sad/csr/advertiser?id=" + adv.getId() + "'>" + adv.getProfileName()+ "</th><th>" + adv.getFirstName() + " " + adv.getLastName() + "</th><th>" + adv.getEmail() + "</th>" + "</th><th>" + adv.getPhone() + "</th></tr>";
                }
                html += "</table>";
                return html;
            } else {
                return error();
            }

       
    }
    
    
    
    
    @GET
    @Path("/advertiser")
    @Produces(MediaType.TEXT_HTML)
    public String getAdvertiser(@QueryParam("id") long id) throws FileNotFoundException, ParseException{ 
        advertiserService = new AdvertiserService();
        Advertiser advertiser = advertiserService.getAdvertiser(id);
            
        html += "</ul></nav></div><h2 class='lefto'><b>Listings for: '" + advertiser.getFirstName() + " " + advertiser.getLastName() + "'</h2><br>";
        if (!advertiser.getListings().isEmpty()) {
            html += "<table class=\"table\"><thead>\n"
                    + "        <tr>\n"
                    + "            <th>Status</th>\n"
                    + "            <th>Title</th>\n"
                    + "            <th>Price</th>\n"
                    + "        </tr>\n"
                    + "        </thead>\n"
                    + "        <tbody>";
            html += "<tr class=\"success\"><th>Active Listings</th><th></th><th></th></tr>" ;
            for (Listing listing : advertiser.getListings()) {
                if(listing.getEndDate().equalsIgnoreCase("still available")){
                    html += "<tr><th></th><th>"  + listing.getTitle() + "</th><th>" + listing.getPrice() + "</th></tr>";
                }
            }
            html += "<tr class=\"danger\"><th>Past Listings</th><th></th><th></th></tr>" ;
            for (Listing listing : advertiser.getListings()) {
                if(!listing.getEndDate().equalsIgnoreCase("still available")){
                    html += "<tr><th></th><th>"  + listing.getTitle() + "</th><th>" + listing.getPrice() + "</th></tr>";
                }
            }
        } else {
            html += "<p>No Listings Found!</p><br><a href=\"http://localhost:8080/sad/csr\"><button type=\"submit\" class=\"btn btn-default\">Go Back</button></html>";
        }
        return html += "</tbody></table>";
    }
    
     
    

    @GET
    @Path("/search/error")
    @Produces(MediaType.TEXT_HTML)
    public String error() {
        return "<!DOCTYPE html>\n"
                + "<html> \n"
                + "    <head>\n"
                + "        <meta charset=\"utf-8\">\n"
                + "        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n"
                + "        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n"
                + "        <!-- Bootstrap core CSS -->\n"
                + "        <link href=\"http://localhost:8080/css/bootstrap.min.css\" rel=\"stylesheet\">\n"
                + "\n"
                + "        <!-- Custom styles for this template -->\n"
                + "        <link href=\"http://localhost:8080/css/jumbotron-narrow.css\" rel=\"stylesheet\">\n"
                + "        <title>Login Page</title>\n"
                + "    </head> \n"
                + "    <body>\n"
                + "        <div class=\"container\">\n"
                + "            <div class=\"header clearfix\">\n"
                + "                <nav>\n"
                + "                    <ul class=\"nav nav-pills pull-right\">\n"
                + "                        <li role=\"presentation\" class=\"\"><a href=\"http://localhost:8080/sad/directory\">Home</a></li>\n"
                + "                    </ul>\n"
                + "                </nav>\n"
                + "            </div>\n"
                + "        <h1>Sorry, No results found! </h1><p>Go back and try again<br><br><a href=\"http://localhost:8080/sad/csr\"><button type=\"submit\" class=\"btn btn-default\">Go Back</button></html>";
    }

}
